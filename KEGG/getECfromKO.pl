#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Getopt::Long;
use LWP::Simple;

my $version='0.1';
my $KAASinfile='';
my %ko2pathway;
my %pathway;
my %ko2ec;
my %ec;
my $warnings=0;
my $ECannot='';
my $help='';
my $license='';

GetOptions(
  'help|h|?'    => \$help,
  'warnings=i'  => \$warnings,
  'infile|i=s'  => \$KAASinfile,
  'outfile|o=s' => \$ECannot,
  'license|l'   => \$license,
);

if(!-s $KAASinfile) {
  print "The input file (with protein names and corresponding KOs, separated by TAB) is required.\n";
  &usage();
  exit(1);
}

if($help) {
  &usage();
  exit(0);
}

if($license) {
 &license();
 exit(0);
}

if(!$ECannot) {
  print "The output file name (e.g. --outfile FILE.out) is required.\n";
  &usage();
  exit(1);
}

if (-e $ECannot) {
 print "The output file (\"$ECannot\") already exists!\n";
 exit(1);
}

if (!($warnings == 0 || $warnings == 1)) {
 print "Invalid integer for warnings (only 0 (FALSE) or 1 (TRUE) are allowed)!\n";
 exit(1);
}

#Get map between KO and pathway
my $map_ko_pathway_file="KEGG_map_to_pathway.txt";
my $pathway_names_file="KEGG_pathway_names.txt";

if(!-s $map_ko_pathway_file){
 open OUTK2P, ">$map_ko_pathway_file";
 my $map_ko_pathway_url="http://rest.kegg.jp/link/pathway/ko";
 my $map_ko_pathway_content=get($map_ko_pathway_url);
 my @map_ko_pathway_lines=split(/\n/,$map_ko_pathway_content);
 print STDERR "There are ".scalar(@map_ko_pathway_lines)." in the ko to pathway map\n\n";
 foreach my $l(@map_ko_pathway_lines){
  chomp $l;
  my ($ko,$pathway)=split(/\t/,$l);
  $pathway=~s/^path\://;
  $ko=~s/^ko\://;
  if($pathway=~/^map/){
   $ko2pathway{$ko}{$pathway}=1;
   $pathway{$pathway}=1;
   print OUTK2P "$ko\t$pathway\n";
  }
 }
 close OUTK2P;
}
else{
 open OUTK2P, "$map_ko_pathway_file"; 
 while (<OUTK2P>){
  chomp;
  my ($ko,$pathway)=split(/\t/);
  $ko2pathway{$ko}{$pathway}=1;
  $pathway{$pathway}=1;  
 }
 close OUTK2P;
}

if(!-s $pathway_names_file){
 open PATHNAME, ">$pathway_names_file";
 foreach my $path(keys %pathway){
  my $pathway_url="http://rest.kegg.jp/get/pathway:$path";
  my $pathway_content=get($pathway_url);
  foreach my $l(split(/\n/,$pathway_content)){
   if($l=~/^NAME\s+(.+)$/){
    $pathway{$path}=$1;
     print PATHNAME "$path\t$1\n";
   }
  }
 }
 close PATHNAME;
}
else{
 open PATHNAME, "$pathway_names_file";
 while(<PATHNAME>){
  chomp;
  my ($pathway,$pathway_name)=split(/\t/);
  $pathway{$pathway}=$pathway_name;
 }
 close PATHNAME;
}

#Get map between KO and enzyme codes
my $map_ko_ec_file="KEGG_map_to_EC.txt";

if(!-s $map_ko_ec_file){
 open OUTK2E, ">$map_ko_ec_file"; 
 my $map_ko_ec_url="http://rest.kegg.jp/link/enzyme/ko";
 my $map_ko_ec_content=get($map_ko_ec_url);
 my @map_ko_ec_lines=split(/\n/,$map_ko_ec_content);
 print STDERR "There are ".scalar(@map_ko_ec_lines)." in the ko to enzyme code map\n\n";

 foreach my $l(@map_ko_ec_lines){
  chomp $l;
  my ($ko,$ec)=split(/\t/,$l);
  $ko=~s/^ko\://;
  if($ec=~/^ec:/){
   $ko2ec{$ko}{$ec}=1;
   $ec{$ec}=1;
   print OUTK2E "$ko\t$ec\n";
  }
  else{
   die "EC $ec in wrong format. Check line $l\n";
  }
 }
 close OUTK2E;
}
else{
 open OUTK2E, "$map_ko_ec_file"; 
 while(<OUTK2E>){
  chomp;
  my ($ko,$ec)=split(/\t/);
  $ko2ec{$ko}{$ec}=1;
  $ec{$ec}=1; 
 }
 close OUTK2E;
}

open OUTFILE, ">$ECannot";
open(KAASIN,$KAASinfile);

while(<KAASIN>) {
 next if /^#/;
 my $pathway_id="";
 my $pathway_name="";
 my $ec="";
 chomp;
 my @fields = split(/\t/,$_);
 my $EC='';
 if(scalar(@fields) == 2) {
  my ($prot,$KO)=@fields;
  $ec=$ko2ec{$KO}?join(",",keys %{$ko2ec{$KO}}):'NA';
  if($ko2pathway{$KO}){
   foreach my $path(keys %{$ko2pathway{$KO}}){
    print OUTFILE "$prot\t$KO\t$ec\t$path\t$pathway{$path}\n";
   }
  }
  else{
   print OUTFILE "$prot\t$KO\t$ec\tNA\tNA\n";
  }
 }
 else{
  warn("Skipping line with more than 2 fields\nCheck you input\n");
 }
}

close(KAASIN);
close(OUTFILE);

sub usage {
    print STDERR "$0 version $version, Diego Mauricio Riano Pachon, Renato Augusto Correa dos Santos\n";
    print STDERR <<EOF;

NAME
    $0

USAGE
    $0 --infile INFILE.txt --outfile OUTFILE.txt

OPTIONS
    --infile,	-i	Input annotation file.					REQUIRED
    --outfile,  -o      Annotation (protein name, and corresponding KO and EC)  REQUIRED
    --help,	-h	This help.
    --license   -l      License.

EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2014 Diego Mauricio Riano Pachon, Renato Augusto Correa dos Santos
http://bce.bioetanol.org.br
e-mail: diriano\@gmail.com; diego.riano\@bioetanol.org.br; renatoacsantos\@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}
