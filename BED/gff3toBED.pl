#!/usr/bin/perl

###################################################
##This script takes a GFF3 formated file
## and creates a BED file compatible for use
##  with RSeqQC
###################################################

use strict;
use warnings;
use Getopt::Long;

my $GFF3file='';
my $BEDfile='';
my %transcriptsSpan;
my $transcripts='';
my $license='';
my $help='';
my $version='1.0';

##########################################
#Get options from the user
##########################################
GetOptions(
    'license|l'       => \$license,
    'help|h|?'        => \$help,
    'infile|i=s'      => \$GFF3file,
    'outfile|o=s'     => \$BEDfile
);

##########################################
#Check user input
##########################################

if ($help){
 &usage();
 exit(1);
}
if ($license){
 &license();
 exit(1);
}
if (!$GFF3file){
 print STDERR "FATAL: You must provide an input file (GFF3).\n";
 &usage();
 exit(1);
}
if (!$BEDfile){
 print STDERR "FATAL: You must provide a name for output file (BED).\n";
 &usage();
 exit(1);
}
if (-e $BEDfile){
 print STDERR "FATAL: output file already exists.\n";
 &usage();
 exit(1);
}
 
$transcripts=parseGFF3($GFF3file);
createBED($transcripts);
 
sub createBED{
 my $tu=shift;
 foreach my $chr(keys %{$tu}){
  foreach my $strand(keys %{$$tu{$chr}}){
    foreach my $id(keys %{$$tu{$chr}{$strand}}){
     if(!$transcriptsSpan{$id}{'start'}){print STDERR "Error FATAL: Start is not a number, got an undefined value. Check your input for $id strand:$strand\n"; next;} #This happens when there is no e.g., mRNA line for a gene. We need that line to get the full length span of the gene.
     my $bedStart=$transcriptsSpan{$id}{'start'}-1;
     my @starts=();
     my @lengths=();
     my $count=0;
     foreach my $start(sort {$a <=> $b} keys %{$$tu{$chr}{$strand}{$id}}){
      $starts[$count] =($start-1)-$bedStart;
      $lengths[$count]=$$tu{$chr}{$strand}{$id}{$start};
      $count++;
     }
     open(BEDFILE, ">>", $BEDfile);
     print BEDFILE "$chr\t$bedStart\t$transcriptsSpan{$id}{'end'}\t$id\t.\t$strand\t$bedStart\t$transcriptsSpan{$id}{'end'}\t0\t".scalar(keys %{$$tu{$chr}{$strand}{$id}})."\t".join(',',@lengths).",\t".join(',',@starts).",\n";
     close(BEDFILE);
    }
  }
 }
}

sub parseGFF3{
 my %transcripts;
 my $gffFile=shift;
 open GFF3, $gffFile;
 while(<GFF3>){
  next if /^#/;
  chomp;
  my ($chr,$source,$feature,$start,$end,undef,$strand,$phase,$notes)=split(/\t/);
  if($feature eq 'exon'){
   my ($idfeat,$parent)=split(";",$notes);
   $parent=~s/Parent=//;
   my ($start2,$end2)=();
   if($start<$end){
    $start2=$start;
    $end2=$end;
   }
   else{
    $start2=$end;
    $end2=$start;
   }
   my $length=($end2-$start2)+1;
   $transcripts{$chr}{$strand}{$parent}{$start2}=$length;
  }
  elsif($feature=~/^.+RNA$/){
   my ($idfeat,$parent)=split(";",$notes);
   $idfeat=~s/ID=//;
   $transcriptsSpan{$idfeat}{'start'}=$start;
   $transcriptsSpan{$idfeat}{'end'}  =$end;
  }
 }
 close GFF3;
 return \%transcripts
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2014, 2015 Diego Mauricio Riaño Pachón, Renato Augusto Corrêa dos Santos\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0 generates a BED file using a GFF3 annotation file as input.

USAGE
    $0 --infile annotation.gff3 --outfile annotation.bed

OPTIONS
    --infile   -i        An annotation file (GFF3 format)     REQUIRED
    --outfile  -o        A name for outfile (BED format)      REQUIRED
    --debug              default 0                            OPTIONAL
    --help     -h        This help.
    --license  -l        License.

EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2014, 2015 Diego Mauricio Riaño Pachón, Renato Augusto Corrêa dos Santos
http://bce.bioetanol.cnpem.br
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

