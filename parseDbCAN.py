#!env python3

import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("infile", type=str,
                    help="provide the name of the sequence input file")

args=parser.parse_args()


with open(args.infile, "rt") as handle:
 for line in handle:
  line=line.rstrip()
  if not line.startswith("Gene ID"):
    cazymes={}
    protID,ECNumber,hmmerRes,eCamiRes,DiamondRes,NT=line.split("\t")
    if hmmerRes!='-':
      hmmerItems=hmmerRes.split('+')
      for hmmerItem in hmmerItems:
        hmmerItem=re.sub('\([0-9-]*\)','',hmmerItem).split('_')[0]
        # print(hmmerItem)
        if hmmerItem not in cazymes:
          cazymes[hmmerItem]={}
        cazymes[hmmerItem]['hmmer']=1
        
    if eCamiRes!='-':
      eCamiItems=eCamiRes.split('+')
      for eCamiItem in eCamiItems:
        eCamiItem=re.sub('\([0-9-]*\)','',eCamiItem).split('_')[0]
        # print(eCamiItem)
        if eCamiItem not in cazymes:
          cazymes[eCamiItem]={}
        cazymes[eCamiItem]['eCAMI']=1
    if DiamondRes!='-':
      diamondItems=DiamondRes.split('+')
      for diamondItem in diamondItems:
        diamondItem=re.sub('\([0-9-]*\)','',diamondItem).split('_')[0]
        # print(diamondItem)
        if diamondItem not in cazymes:
          cazymes[diamondItem]={}
        cazymes[diamondItem]['diamond']=1
    for fam in cazymes.keys():
      #The family should have been predicted by at least two tools
      if len(cazymes[fam])>1:
        # print(f'{protID}\t{fam}\t{cazymes[fam]}')
        classCazy=re.sub('[0-9]+$','',fam)
        print(f'{protID}\t{classCazy}\t{fam}')