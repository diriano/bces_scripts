#!/usr/bin/perl

use strict;
use warnings;

my %transcripts2clusters;
my %clusters2transcripts;
my %representativeTranscript;
parseCDHIT($ARGV[0]);

my $OUTTBL1=$ARGV[0].".tbl";
my $OUTTBL2=$ARGV[0].".ids";
my $OUTTBL3=$ARGV[0].".tx2gene";

open OUTTBL1, ">$OUTTBL1";
open OUTTBL2, ">$OUTTBL2";
open OUTTBL3, ">$OUTTBL3";
print OUTTBL2 "ClusterID\tRepresentativeSeq\tAllSeqIds\n";
foreach my $cluster(keys %clusters2transcripts){
 print OUTTBL2 "$cluster\t$representativeTranscript{$cluster}\t".join("|",keys $clusters2transcripts{$cluster})."\n";
 foreach my $seq(keys %{$clusters2transcripts{$cluster}}){
  my $rep='';
  if($representativeTranscript{$cluster} eq $seq){
   $rep="true";
  }
  else{
   $rep="false";
  }
  print OUTTBL1 "$cluster\t$seq\t$rep\n";
  print OUTTBL3 "$seq\t$representativeTranscript{$cluster}\n";
 }
}
close OUTTBL1;
close OUTTBL2;
close OUTTBL3;

sub parseCDHIT{
 my $clusterFile=shift;
 open CDHIT, $clusterFile;
 my $clusterId='';
 while(<CDHIT>){
  chomp;
  if (/^>Cluster (\d+)$/){
   $clusterId='cluster_'.$1;
  }
  else{
   my @f1=split(/,/);
   my @f2=split(/ /,$f1[1]);
   my $seqId=$f2[1];
   $seqId=~s/^>//;
   $seqId=~s/\.+$//;
   if(/ \*$/){
    $representativeTranscript{$clusterId}=$seqId;
   }
   $transcripts2clusters{$seqId}=$clusterId;
   $clusters2transcripts{$clusterId}{$seqId}=1;
  }
 }
 close CDHIT;
}
