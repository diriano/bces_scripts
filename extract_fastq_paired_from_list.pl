#!/usr/bin/perl

##########################################
##This script takes a couple of fastq files from a paired-end rnd a list of IDS
## iit creates a couple of FASTq files with only the IDs in the list file
## DMRP Jan 2015
##########################################

use strict;
use warnings;
use diagnostics;
use Bio::SeqIO;
use Bio::Index::Fastq;
use Getopt::Long;
use File::Basename;

my $version='0.5';
my $infile1='';
my $infile2='';
my $license='';
my $listfile='';
my $omitslashnum=0;
my $help='';
my $debug=0;

GetOptions(
    'license|l'    => \$license,
    'help|h|?'     => \$help,
    'debug|d:i'    => \$debug,
    'infile1|1=s'  => \$infile1,
    'infile2|2=s'  => \$infile2,
    'list=s'       => \$listfile,
    'omittrailingslashnumber' => \$omitslashnum,
);

if(!-s $infile1){
 print STDERR "You must provide a fastq input file\n";
 usage();
 exit 1;
}
if(!-s $infile2){
 print STDERR "You must provide a fastq input file\n";
 usage();
 exit 1;
}
if(!-s $listfile){
 print STDERR "You must provide a file with a list of IDs\n";
 usage();
 exit 1;
}
if($help){
 usage();
  exit 0;
}

###Indexing input files
my $infile1inx= $infile1.'.inx';
my $infile2inx= $infile2.'.inx';

my $infile1_objinx = Bio::Index::Fastq->new('-write_flag'=> 1,
                                            '-filename'  => $infile1inx);
my $infile2_objinx = Bio::Index::Fastq->new('-write_flag'=> 1,
                                            '-filename'  => $infile2inx);

#$infile1_objinx->id_parser( \&fastq_rm_slash_makeid );

$infile1_objinx ->make_index($infile1);
$infile2_objinx ->make_index($infile2);

my $outfile1=$infile1.".sublist.fastq";
my $outfile2=$infile2.".sublist.fastq";

my $outfile1_obj = Bio::SeqIO->new(-format    => 'fastq',
                                   -file      => ">$outfile1");

my $outfile2_obj = Bio::SeqIO->new(-format    => 'fastq',
                                   -file      => ">$outfile2");

###########################################################
###Process file with list of IDs, one ID per line
###########################################################
open IDs, $listfile;
while(<IDs>){
 chomp;
 my ($id)=split(/ /);
 my $seq_obj1 = $infile1_objinx->fetch($id);
 my $seq_obj2 = $infile2_objinx->fetch($id);
 $outfile1_obj->write_seq($seq_obj1);
 $outfile2_obj->write_seq($seq_obj2); 
}

unlink $infile1inx;
unlink $infile2inx;

sub fastq_rm_slash_makeid{
 #This version is not using this TODO
 #This take care of FASTQ IDs where there is a trailing \1 or \2, or . . ., i.e., it just removes it.
 my $id=shift;
 $id=~s/\@//;
 $id=~s/\\[0-9]$//;
 return $id;
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2015 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   Produces really paired-end fastq files, i.e., omit unpaired reads 

USAGE
    $0 -1 R1.fastq -2 R2.fastq --list file.list.txt

OPTIONS
    --infile1,1    R1 fastq input file   REQUIRED
    --infile2,2    R2 fastq input file   REQUIRED
    --list         List of IDs to keep   REQUIRED
    --help,       -h     This help.
    --license,    -l     License.

EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2015 Diego Mauricio Riaño Pach�
http://bce.bioetanol.cnpem.br
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

