#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Bio::DB::Fasta;
use Bio::Seq;
use Bio::SeqIO;

my $version = 0.1;
my $genome_file= '';
my $gff3_file= '';
my $help='';
my $license='';

GetOptions
  (
   'genome=s'       => \$genome_file,
   'gff3=s'         => \$gff3_file,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $genome_file){
 warn "FATAL: You must provide a file with the genome sequence in fasta format\n";
 usage();
 exit 1;
}
if(!-s $gff3_file){
 warn "FATAL: You must provide a GFF3 file\n";
 usage();
 exit 1;
}
if($gff3_file!~/\.gff3$/){
 warn "FATAL: The GFF3 file must have extension .gff3\n";
 usage();
 exit 1;
}
#index genome file;
my $inx = Bio::DB::Fasta->new($genome_file);

open GFF3, "$gff3_file";
my @gff3=<GFF3>;
close GFF3;
my $transcriptID='';
my $geneID='';
my $CDS='';
my $PEP='';

my $CDS_outfile = $gff3_file;
$CDS_outfile=~s/.gff3$//;
$CDS_outfile=$CDS_outfile.".CDS.fasta";
my $CDS_outfile_obj=Bio::SeqIO->new( '-format' => 'fasta' , -file => ">$CDS_outfile");

my $PEP_outfile = $gff3_file;
$PEP_outfile=~s/.gff3$//;
$PEP_outfile=$PEP_outfile.".PEP.fasta";
my $PEP_outfile_obj=Bio::SeqIO->new( '-format' => 'fasta' , -file => ">$PEP_outfile");

my $Gene_outfile = $gff3_file;
$Gene_outfile=~s/.gff3$//;
$Gene_outfile=$Gene_outfile.".Gene.fasta";
my $Gene_outfile_obj=Bio::SeqIO->new( '-format' => 'fasta' , -file => ">$Gene_outfile");

for (my $i=0;$i<@gff3;$i++){
 chomp $gff3[$i];
 my @fields=split(/\t/,$gff3[$i]);
 if(@fields == 9){
  if($fields[2] eq 'gene'){
   $geneID=$fields[-1]; 
   $geneID=~s/ID=//;
#   my $chr_obj=$inx->get_Seq_by_id($fields[0]);
   my $geneSeq='';
   if($fields[6] eq '-'){
#    $geneSeq=$chr_obj->subseq($fields[4] => $fields[3]);
    $geneSeq=$inx->seq($fields[0],$fields[4] => $fields[3]);
   }
   else{
#    $geneSeq=$chr_obj->subseq($fields[3] => $fields[4]);
    $geneSeq=$inx->seq($fields[0],$fields[3] => $fields[4]);
   }
   my $Gene_obj=Bio::PrimarySeq->new (-seq      => $geneSeq,
                                      -id       => $geneID,
                                      -alphabet => 'dna'
                                    );
   $Gene_outfile_obj->write_seq($Gene_obj);
  }
  elsif($fields[2] eq 'transcript'){
   ($transcriptID,undef)=split(/;/,$fields[-1]);
   $transcriptID=~s/ID=//;
  }
 }
 elsif($gff3[$i]=~/^# coding sequence = \[(.+)$/){
  $CDS=$1;
  for (my $j=$i+1;$j<@gff3;$j++){
   chomp $gff3[$j];
   if($gff3[$j]=~/^# protein sequence = /){
    last;
   }
   else{
    $CDS.=$gff3[$j]
   }
  }
  $CDS=~s/[ \]#]//g;
 }
 elsif($gff3[$i]=~/^# protein sequence = \[(.+)/){
  $PEP=$1;
  for (my $j=$i+1;$j<@gff3;$j++){
   chomp $gff3[$j];
   if($gff3[$j]=~/^# end gene/){
    last;
   }
   else{
    $PEP.=$gff3[$j]
   }
  }
  $PEP=~s/[ \]#]//g;
 }
 if($gff3[$i]=~/^# end gene /){
   print "$geneID\t$transcriptID\n";
   my $CDS_obj=Bio::PrimarySeq->new ( -seq      => $CDS,
                                      -id       => $transcriptID,
                                      -alphabet => 'dna'
                                    );
   my $PEP_obj=Bio::PrimarySeq->new ( -seq      => $PEP,
                                      -id       => $transcriptID,
                                      -alphabet => 'protein'
                                    );
   $CDS_outfile_obj->write_seq($CDS_obj);
   $PEP_outfile_obj->write_seq($PEP_obj);
   $transcriptID='';
   $geneID='';
   $CDS='';
   $PEP='';
 }
#YCS105_SCAFF0002        AUGUSTUS        gene    1       213     0.99    +       .       ID=g1
#YCS105_SCAFF0002        AUGUSTUS        transcript      1       213     0.99    +       .       ID=g1.t1;Parent=g1
#YCS105_SCAFF0002        AUGUSTUS        CDS     1       210     0.99    +       0       ID=g1.t1.cds;Parent=g1.t1
#YCS105_SCAFF0002        AUGUSTUS        stop_codon      211     213     .       +       0       Parent=g1.t1
## coding sequence = [ttccattatgaagctacaaaagatccattaggactagttaaattgattaaaagtcatggtattagagctgcttgtgcta
## tcaaaccaggtactcctgttgatgttttgtttgaattagctccatatttagatatggccttagttatgactgttgaacctggttttggtggtcaaaaa
## ttcatggctgaggttaacaactatcaaattctttaa]
## protein sequence = [FHYEATKDPLGLVKLIKSHGIRAACAIKPGTPVDVLFELAPYLDMALVMTVEPGFGGQKFMAEVNNYQIL]
## end gene g1
#
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2013 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   Parses an Augustus 3.1 GFF3 file and extracts protein and CDS sequences that are present in the GFF file. 
         It also extracts gene position and extract the corresponding gene sequence from the genome file.

USAGE
    $0 --genome genome.fasta --gff genome.gff

OPTIONS
    --genome           Genome sequence.            REQUIRED
    --gff3             GFF file.                   REQUIRED
    --help,     -h     This help.
    --license,  -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2015 Diego Mauricio Riaño Pachón
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

