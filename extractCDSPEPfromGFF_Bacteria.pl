#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Getopt::Long;
use Bio::DB::Fasta;
use Bio::Seq;
use Bio::SeqIO;
## Set options

my $version = 0.1;
my $genome_file= '';
my $gff_file= '';
my $help='';
my $license='';

GetOptions
  (
   'genome=s'       => \$genome_file,
   'gff=s'          => \$gff_file,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $genome_file){
 warn "FATAL: You must provide a file with the genome sequence in fasta format\n";
 usage();
 exit 1;
}
if(!-s $gff_file){
 warn "FATAL: You must provide a GFF file\n";
 usage();
 exit 1;
}
if($gff_file!~/\.gff$/){
 warn "FATAL: The GFF file must have extension .gff\n";
 usage();
 exit 1;
}
if($genome_file!~/\.(fasta|fsa|fa)$/){
 warn "FATAL: The Genome file must have extension .fasta|fa|fsa\n";
 usage();
 exit 1;
}

#########################
##Index Genome File
#########################

my $genome_inx=Bio::DB::Fasta->new($genome_file);


#########################
##Create output file and
## BioPerl object to handle these
#########################

my $peptide_out=$genome_file.".pep.fasta";

my $cds_out=$genome_file.".cds.fasta";

my $peptideOutObj = Bio::SeqIO->new(-file => ">$peptide_out",
                                    -format => 'Fasta'
                                    );

my $cdsOutObj = Bio::SeqIO->new(-file => ">$cds_out",
                                -format => 'Fasta'
                                );


#########################
##Process  GFF file
#########################

open GFF, $gff_file;
while(<GFF>){
 chomp;
 my @fields=split(/\t/);
 if($fields[2] eq 'CDS'){
  my $seq=$genome_inx->seq($fields[0],$fields[3] => $fields[4]);
  my ($geneID)=split(/;/,$fields[-1]);
  $geneID=~s/ID=//;
  $geneID=~s/-mRNA-1:cds//;
  my $newSeqObj=Bio::Seq->new(-seq        => $seq,
                              -display_id => $geneID,
                              -alphabet   => 'dna');
  if($fields[6] eq '-'){
   my $revCDS=$newSeqObj->revcom;
   $cdsOutObj->write_seq($revCDS);
   my $protObj=$revCDS->translate(-codontable_id => 11);
   $peptideOutObj->write_seq($protObj);
  }
  else{
   $cdsOutObj->write_seq($newSeqObj);
   my $protObj=$newSeqObj->translate(-codontable_id => 11);
   $peptideOutObj->write_seq($protObj);
  }
#  print $seq."\t$geneID\n";
 }
}


sub usage{
    print STDERR "$0 version $version, Copyright (C) 2013 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   extracts CDS regions form the annotation of a bacterial genome and create a trnascript and peptide file.

USAGE
    $0 --genome genome.fasta --gff genome.gff

OPTIONS
    --genome           Genome sequence.            REQUIRED
    --gff              GFF file.                   REQUIRED
    --help,     -h     This help.
    --license,  -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2014 Diego Mauricio Riaño Pachón
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}
