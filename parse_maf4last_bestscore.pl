#!/usr/bin/perl
#
use warnings;
use strict;

open IN , $ARGV[0];
my %alns;
my $count_aln=0;
my $score='';
my %sawQuery;
my ($sid,$sstart_base0,$srun,$sstrand,$slength,$saln_str,$qid,$qstart_base0,$qrun,$qstrand,$qlength,$qaln_str)=();
while(<IN>){
 chomp;
 next if (/^#/);
 if(/^a score=(.+)$/){
  $count_aln++;
  $score=$1;
#  $alns{$count_aln}{'score'}=$score;
  ($sid,$sstart_base0,$srun,$sstrand,$slength,$saln_str,$qid,$qstart_base0,$qrun,$qstrand,$qlength,$qaln_str)=();
 }
 elsif(/^s sp.*/){
  #This is the subject sequence
  (undef,$sid,$sstart_base0,$srun,$sstrand,$slength,$saln_str)=split(/ +/);
 }
 elsif(/^s (A[0-9].A[0-9].[A-Z]\d+)/){
  #This is the query sequence
  my $qID=$1;
  my @fields=split(/ +/);
  (undef,$qid,$qstart_base0,$qrun,$qstrand,$qlength,$qaln_str)=split(/ +/);
  if(!$sawQuery{$qid}){
   $sawQuery{$qid}=1;
   $alns{$qid}{'score'}=$score;
   $alns{$qid}{'subject'}{'id'}=$sid;
   $alns{$qid}{'subject'}{'start'}=$sstart_base0+1;
   $alns{$qid}{'subject'}{'end'}=$sstart_base0+$srun;
   $alns{$qid}{'subject'}{'length'}=$slength;
   $alns{$qid}{'subject'}{'aln_str'}=$saln_str;
   $alns{$qid}{'query'}{'id'}=$qid;
   $alns{$qid}{'query'}{'start'}=$qstart_base0+1;
   $alns{$qid}{'query'}{'end'}=$qstart_base0+$qrun;
   $alns{$qid}{'query'}{'length'}=$qlength;
   $alns{$qid}{'query'}{'aln_str'}=$qaln_str;
  }
  else{
  # print STDERR "$score,$sid,$sstart_base0,$srun,$sstrand,$slength,$saln_str,$qid,$qstart_base0,$qrun,$qstrand,$qlength,$qaln_str\n";
   if($alns{$qid}{'score'} < $score){
    $alns{$qid}{'score'}=$score;
    $alns{$qid}{'subject'}{'id'}=$sid;
    $alns{$qid}{'subject'}{'start'}=$sstart_base0+1;
    $alns{$qid}{'subject'}{'end'}=$sstart_base0+$srun;
    $alns{$qid}{'subject'}{'length'}=$slength;
    $alns{$qid}{'subject'}{'aln_str'}=$saln_str;
    $alns{$qid}{'query'}{'id'}=$qid;
    $alns{$qid}{'query'}{'start'}=$qstart_base0+1;
    $alns{$qid}{'query'}{'end'}=$qstart_base0+$qrun;
    $alns{$qid}{'query'}{'length'}=$qlength;
    $alns{$qid}{'query'}{'aln_str'}=$qaln_str;
   }
  }
 }
}

foreach my $aln(keys %alns){
 my @alnQueryBases=split(//,$alns{$aln}{'query'}{'aln_str'});
 my @alnSubjectBases=split(//,$alns{$aln}{'subject'}{'aln_str'});
 my $matches=0;
 my $mismatches=0;
 for (my $i=0;$i<@alnQueryBases;$i++){
  if($alnQueryBases[$i] eq $alnSubjectBases[$i]){
   $matches++;
  }
  else{
   $mismatches++;
  }
 }
 my $pident=($matches/length($alns{$aln}{'query'}{'aln_str'}))*100;
 my $gapopenQuery = () = $alns{$aln}{'query'}{'aln_str'} =~ /-+/g;
 my $gapopenSubject = () = $alns{$aln}{'subject'}{'aln_str'} =~ /-+/g;
 my $gapopen=$gapopenQuery+$gapopenSubject;
 print "$alns{$aln}{'query'}{'id'}\t$alns{$aln}{'subject'}{'id'}\t$pident\t".length($alns{$aln}{'query'}{'aln_str'})."\t$mismatches\t$gapopen\t$alns{$aln}{'query'}{'start'}\t$alns{$aln}{'query'}{'end'}\t$alns{$aln}{'subject'}{'start'}\t$alns{$aln}{'subject'}{'end'}\tevalue\t$alns{$aln}{'score'}\n";
}
