#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;

my $version="1.0";
my $license='';
my $help='';
my $infile='';

#Get input from user
#
GetOptions(
 'license|l'   => \$license,
 'help|h|?'    => \$help,
 'infile|i=s'  => \$infile
);

if($help){
 &usage;
 exit 1
}
if($license){
 &license;
 exit 1
}
if(!-s $infile){
 print STDERR"\n\tFATAL: You must provide an infile, ithe LOG file in the SortMErNA run.\n\n";
 &usage;
 exit 0;
}

my $outfile=$infile;

open IN, $infile;
my ($sample,$rRNAperc,$nonrRNAperc)=('','','');
print "^ Sample ^ rRNA Percent ^ nonrRNA percent ^ \n";
while(<IN>){
 chomp;
 if(/^ Command: \/bioinf\/progs\/sortmerna-2.0-linux-64\/sortmerna --reads (.+).pairedmerged.fastq -a/){
  $sample=$1;
 }
# elsif(/^\s+Total reads passing E-value threshold = (\d+) \(([\d.%])\)/){
 elsif(/^\s+Total reads passing E-value threshold = (\d+) \((.+)\)/){
  $rRNAperc=$2;
 }
 elsif(/^\s+Total reads failing E-value threshold = (\d+) \((.+)\)/){
  $nonrRNAperc=$2;
 }
}

print "| $sample | $rRNAperc | $nonrRNAperc | \n";


#    Total reads failing E-value threshold = 164301 (7.86%)
#    Minimum read length = 60
#    Maximum read length = 151
#    Mean read length = 140
# By database:
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/silva-bac-16s-id90.fasta                17.10%
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/silva-bac-23s-id98.fasta                74.06%
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/silva-arc-16s-id95.fasta                0.00%
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/silva-arc-23s-id98.fasta                0.07%
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/silva-euk-18s-id95.fasta                0.16%
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/silva-euk-28s-id98.fasta                0.69%
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/rfam-5.8s-database-id98.fasta           0.05%
#    /bioinf/progs/sortmerna-2.0-linux-64/rRNA_databases/rfam-5s-database-id98.fasta             0.00%
#(END) 

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2015 Diego Mauricio Riano Pachon\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0  extract summary info form the SortMeRNA log file

USAGE
    $0 --infile sormerna.log

OPTIONS
    --infile      -i    LOG file SortMErNA       REQUIRED
    --help        -h    This help.
    --license     -l    License.

EOF
}
sub license{
    print STDERR <<EOF;

Copyright (C) 2015 Diego Mauricio Riaño Pach<C3>
http://bce.bioetanol.cnpem.br
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

