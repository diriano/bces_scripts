#!/usr/bin/perl

####################################
## Simply count monomer in string
## DMRP 05.08.2016
####################################
use strict;
use warnings;
use Bio::Tools::SeqStats;
use Bio::DB::Fasta;

if (!@ARGV){die}
foreach my $file(@ARGV){
 my $db=Bio::DB::Fasta->new($file);
 my $stream  = $db->get_PrimarySeq_stream;
 while (my $seq = $stream->next_seq) {
   my $hash_ref = Bio::Tools::SeqStats->count_monomers($seq);
   print $seq->id."\t";
   foreach my $base (sort keys %{$hash_ref}) {
            print $base. "\t". $hash_ref->{$base},"\t";
   }
   print "\n";
 }
}
