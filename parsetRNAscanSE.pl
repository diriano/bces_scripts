#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;

## Set options

my $version = 0.1;
my $help='';
my $license='';
my $trnascanse_file='';
my $genome_file= '';

GetOptions
  (
   'genome=s'       => \$genome_file,
   'trnascanse=s'   => \$trnascanse_file,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $trnascanse_file){
 warn "FATAL: You must provide a file with the tRNAscan-SE results/prediction\n";
 usage();
 exit 1;
}
if(!-s $genome_file){
 warn "FATAL: You must provide a file with the genome sequences\n";
 usage();
 exit 1;
}

open TRNA, "$trnascanse_file" or die "cannot open $trnascanse_file";
my $seqId='';
my $count_tRNA;
my $count_exons=1;
while(<TRNA>){
 chomp;
# print "$_\n";
 next if(/^(Sequence|Name|-)/);
 s/ //g;
 $count_tRNA++;
 my ($seqId,$tRNAnumber,$pstart,$pend,$tRNAtype,$codon,$pintronStart,$pintronEnd,$score)=split(/\t/);
 my ($start,$end,$strand)=($pstart>$pend) ? ($pend,$pstart,'-') : ($pstart,$pend,'+');
 my ($intronStart,$intronEnd)=($pintronStart>$pintronEnd) ? ($pintronEnd,$pintronStart) : ($pintronStart,$pintronEnd);
 my $id=$seqId."-tRNAscan-SE-tRNA-".$tRNAtype."-".$codon."-".$count_tRNA;
 print "$seqId\ttRNAscan-SE-1.3.1\ttRNA\t$start\t$end\t$score\t$strand\t.\tID=$id;Name=$id;Type=$tRNAtype\n";
 if($intronStart==0 && $intronEnd==0){
  my $exonId=$id.":exon:$count_exons";
  $count_exons++;
  print "$seqId\ttRNAscan-SE-1.3.1\texon\t$start\t$end\t$score\t$strand\t.\tID=$exonId;Parent=$id\n";
 }
 else{
  my $firstExonId=$id.":exon:$count_exons";
  $count_exons++;
  my $secondExonId=$id.":exon:$count_exons";
  $count_exons++;
  my $firstExonEnd   =$intronStart-1;
  my $secondExonStart=$intronEnd+1;
  print "$seqId\ttRNAscan-SE-1.3.1\texon\t$start\t$firstExonEnd\t$score\t$strand\t.\tID=$firstExonId;Parent=$id\n";
  print "$seqId\ttRNAscan-SE-1.3.1\texon\t$secondExonStart\t$end\t$score\t$strand\t.\tID=$secondExonId;Parent=$id\n";
 }
#scaffold22      maker   gene    52704   55423   .       +       .       ID=maker-scaffold22-snap-gene-0.63;Name=maker-scaffold22-snap-gene-0.63
#scaffold22      maker   mRNA    52704   55423   .       +       .       ID=maker-scaffold22-snap-gene-0.63-mRNA-1;Parent=maker-scaffold22-snap-gene-0.63;Name=maker-scaffold22-snap-gene-0.63-mRNA-1;_AED=0.08;_eAED=0.08;_QI=0|0|0|1|0.5|0.33|3|0|644
#scaffold22      maker   exon    52704   53585   .       +       .       ID=maker-scaffold22-snap-gene-0.63-mRNA-1:exon:4511;Parent=maker-scaffold22-snap-gene-0.63-mRNA-1
#scaffold22      maker   exon    54128   54296   .       +       .       ID=maker-scaffold22-snap-gene-0.63-mRNA-1:exon:4512;Parent=maker-scaffold22-snap-gene-0.63-mRNA-1
#scaffold22      maker   exon    54540   55423   .       +       .       ID=maker-scaffold22-snap-gene-0.63-mRNA-1:exon:4513;Parent=maker-scaffold22-snap-gene-0.63-mRNA-1

#scaffold10      1       502785  502897  SeC     TCA     502821  502861  52.76
#scaffold10      2       390592  390502  Glu     CTC     390555  390537  45.37
#scaffold10      3       331979  331888  Asp     GTC     331942  331923  57.97
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2013 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   converts a tRNAscan-SE standard output file into a GFF3 file.

USAGE
    $0 --genome genome.fasta --trnascanse genome.trnascanse

OPTIONS
    --genome           Genome sequence.            REQUIRED
    --trnascanse       tRNAscan-SE output file.    REQUIRED
    --help,     -h     This help.
    --license,  -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2013 Diego Mauricio Riaño Pachón
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}
