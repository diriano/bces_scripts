#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use Bio::DB::Fasta;
use Bio::SeqIO;
use Bio::PrimarySeq;

my $version = 0.1;
my $transcriptome_file= '';
my $univeccore_file= '';
my $blast_file= '';
my $help='';
my $license='';
my %univec;

GetOptions
  (
   'transcriptome|t=s'  => \$transcriptome_file,
   'univec|u=s'         => \$univeccore_file,
   'blast|b=s'          => \$blast_file,
   'help|h|?'           => \$help,
   'license|l'          => \$license
  )
  or die "failed to parse command line options\n";

if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $transcriptome_file){
 warn "FATAL: You must provide a file with the transcriptome sequences in fasta format\n";
 usage();
 exit 1;
}
if(!-s $univeccore_file){
 warn "FATAL: You must provide a file with the UniVec_Core sequences in fasta format\n";
 usage();
 exit 1;
}
if(!-s $blast_file){
 warn "FATAL: You must provide a file with the BLAST results using as database UniVec_Core and using -outfmt '7 std qlen slen'\n";
 usage();
 exit 1;
}


###Get description files for UniVec sequences

open UNIVEC, $univeccore_file;
while(<UNIVEC>){
 if(/^>/){
  chomp;
  my @f=split(/ /);
  my(undef,undef,$id,undef)=split(/[\|:]/,$f[0]);
  my $desc=join(' ',@f[1..$#f]);
  $univec{$id}=$desc;
 }
}
close UNIVEC;

###Process BLAST results

my %removeHead;
my %removeTail;
my %idsToClean;

open BLAST, $blast_file;
while(<BLAST>){
 next if /^#/;
 chomp;
 my ($queryid, $subjectid, $peridentity, $alignmentlength, $mismatches, $gapopens, $qstart, $qend, $sstart, $send, $evalue, $bitscore, $querylength, $subjectlength)=split(/\t/);
 my (undef,undef,$univecid,undef)=split(/[\|:]/,$subjectid);
 next if ($univec{$univecid} !~ /(illumina|titanium)/i);
# if($qstart==1 && $peridentity>=100){
 if($qstart<=10 && $peridentity>=96){
  $idsToClean{$queryid}=1;
  if($removeHead{$queryid}){
   $removeHead{$queryid}{'end'} = $removeHead{$queryid}{'end'} < $qend ? $qend : $removeHead{$queryid}{'end'};
  }
  else{
   $removeHead{$queryid}{'start'}=1;
   $removeHead{$queryid}{'end'}=$qend;
  }
 }
# if($qend==$querylength && $peridentity>=100){
 if($qend>=$querylength-10 && $peridentity>=96){
  $idsToClean{$queryid}=1;
  if($removeTail{$queryid}){
   $removeTail{$queryid}{'start'} = $removeTail{$queryid}{'start'} > $qstart ? $qstart : $removeTail{$queryid}{'start'};
  }
  else{
   $removeTail{$queryid}{'start'}=$qstart;
   $removeTail{$queryid}{'end'}=$querylength;
  }
 }
}

###Index transcriptome fasta file
my $transcriptome_inx=Bio::DB::Fasta->new($transcriptome_file);
my $outfile=$transcriptome_file.'.cleaned';
my $transcriptome_out=Bio::SeqIO->new(
                              -file   => ">$outfile",
                              -format => 'fasta',
                              );
 
my $stream  = $transcriptome_inx->get_PrimarySeq_stream;
while (my $seq = $stream->next_seq) {
 my $id=$seq->display_id();
 if($idsToClean{$id}){
  my ($start,$end)=('','');
  $start= $removeHead{$id} ? $removeHead{$id}{'end'} : 1;
  $end  = $removeTail{$id} ? $removeTail{$id}{'start'} : $seq->length;
  my $subseq=$seq->subseq($start => $end);
#  print $id."\t$start\t$end\t$subseq\n";
  my $newseq=Bio::PrimarySeq->new ( -seq => $subseq,
                                    -id  => $id,
                                    -alphabet => 'dna'
                                  );
  $transcriptome_out->write_seq($newseq);
 }
 else{
  $transcriptome_out->write_seq($seq);
 }
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2013 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0 Removed remainders of Illumina adapters from the beginning  contig sequences

USAGE
    $0 --transcriptome transcriptome.fasta --univec univec.fasta --blast transcriptome_vs_univec.blastn

OPTIONS
    --transcriptome    Transcriptome fasta file.                            REQUIRED
    --univec           Univec_Core fasta file                               REQUIRED
    --blast            BLAST results file, using -outfmt '7 std qlen slen'  REQUIRED
    --help,     -h     This help.
    --license,  -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2014 Diego Mauricio Riaño Pachón
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}


