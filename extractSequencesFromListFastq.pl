#!/usr/bin/perl

###############################################################
###############################################################
## Script: extractSequences                                  ##
## Will extract a subset of sequences from a multifastq file ##
## The user must provide in a separate file the list of      ##
##  identifiers that must be extracted                       ##
## Author: Diego Mauricio Riaño-Pachón                       ##
## Last Modification: December 16, 2010                      ##
###############################################################

use strict;
use warnings;
use diagnostics;
use Bio::Index::Fastq;
use Bio::SeqIO;
use Getopt::Long;

my $version="0.7";
my $fastqR1file='';
my $fastqR2file='';
my $listfile='';
my $license='';
my $help='';

#Get input from user

GetOptions(
    'license|l'       => \$license,
    'help|h|?'        => \$help,
    'fastqR1file|1=s' => \$fastqR1file,
    'fastqR2file|2=s' => \$fastqR2file,
    'listfile|i=s'    => \$listfile,
);


#Check input from user

if($help){
 &usage;
 exit 1
}
if($license){
 &license;
 exit 1
}
if(!-s $fastqR1file){
 print STDERR "\n\tFATAL:  You must provide a fastq R1 file.\n\n";
 &usage;
 exit 0;
}
if(!-s $fastqR2file){
 print STDERR "\n\tFATAL:  You must provide a fastq R2 file.\n\n";
 print STDERR "We are asuming you have single-end reads\n";
}
if(!-s $listfile){
 print STDERR "\n\tFATAL: You must provide a file with the list of sequence identifiers\n\n";
 &usage;
 exit 0;
}

my $mode='';

if($fastqR1file && !$fastqR2file){
 $mode='se';
}
else{
 $mode='pe';
}

&process();

sub process{
 if ($mode eq 'pe'){
  my $dbinxR1=indexdb($fastqR1file);
  my $dbinxR2=indexdb($fastqR2file);
  #####
  ##Create output file
  #####
  my $outfileR1=$listfile.".R1.fastq";
  my $outfileR2=$listfile.".R2.fastq";
  my $outfileR1_obj = Bio::SeqIO->new('-file'   => ">$outfileR1",
                                      '-format' => 'Fastq');
  my $outfileR2_obj = Bio::SeqIO->new('-file'   => ">$outfileR2",
                                      '-format' => 'Fastq');
 
  foreach my $id(@{getRequestedIdentifiers($listfile)}){
   my $seqR1_obj=$dbinxR1->fetch($id);
   my $seqR2_obj=$dbinxR2->fetch($id);
   $outfileR1_obj->write_seq($seqR1_obj);
   $outfileR2_obj->write_seq($seqR2_obj);
   
  }
 }
 else{
  #Working in single-end mode
  my $dbinxR1=indexdb($fastqR1file);
  #####
  ##Create output file
  #####
  my $outfileR1=$listfile.".R1.fastq"; 
  my $outfileR1_obj = Bio::SeqIO->new('-file'   => ">$outfileR1",
                                          '-format' => 'Fastq');
  foreach my $id(@{getRequestedIdentifiers($listfile)}){
   my $seqR1_obj=$dbinxR1->fetch($id);
   $outfileR1_obj->write_seq($seqR1_obj);
  }
 }
} 
sub indexdb{
 my $file=shift;
 my $inx_file=$file.".inx";
 my $inxObj= Bio::Index::Fastq->new(
        '-filename' => $inx_file,
        '-write_flag' => 1);
 $inxObj->make_index($file);
 return $inxObj;
}

sub getRequestedIdentifiers{
 my $file=shift;
 my @ids=();
 open IN, $file;
 while(<IN>){
  chomp;
  my ($id)=split(/\t/);
  push @ids, $id;
 }
 return(\@ids);
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2010-2015 Diego Mauricio Riano Pachon\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   extracts a subset of sequences from amultifasta file given alist of identifiers.

USAGE
    For paired-end mode:
    $0 --fastqR1file file.R1.fastq --fastqR2file file.R2.fastq --listfile identifierslist.txt

    For single-end mode (e.g., PacBio)
    $0 --fastqR1file file.R1.fastq --listfile identifierslist.txt

OPTIONS
    --fastqR1file -1  Fastq R1 file.                     REQUIRED
    --fastqR2file -2  Fastq R2 file.                     OPTIONAL
    --listfile    -i  List of identifiers, one per line. REQUIRED
    --help        -h  This help.
    --license     -l  License.

EOF
}
sub license{
    print STDERR <<EOF;

Copyright (C) 2010-2015 Diego Mauricio Riaño Pach<C3>
e-mail: diriano\@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

