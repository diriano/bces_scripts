import argparse
import unittest
from io import StringIO


def count_bases_by_transposon_type(input_data, output_file):
    transposons = {}

    with StringIO(input_data) as input_file:
        for line in input_file:
            line = line.strip().split('\t')
            if len(line) < 4:
                continue
            start = int(line[1])
            end = int(line[2])
            transposon = line[3]
            t = transposon.split('#')
            if len(t) < 2:
                continue
            t_type = t[1]
            transposons.setdefault(t_type, 0)
            transposons[t_type] += end - start

    with open(output_file, 'w') as output_file:
        output_file.write('Type\t#Bases\n')
        for t_type, count in transposons.items():
            output_file.write(f'{t_type}\t{count}\n')


def run_count_bases_by_transposon_type(args):
    count_bases_by_transposon_type(args.input, args.output)


class TestCountBasesByTransposonType(unittest.TestCase):
    def setUp(self):
        self.input_data = 'chr1\t10\t30\tTE1_family#Type1\nchr1\t15\t25\tTE2_family#Type2\n'
        self.output_file = 'test_output.txt'
        self.expected_output = 'Type\t#Bases\nType1\t20\nType2\t10\n'

    def tearDown(self):
        pass

    def test_count_bases_by_transposon_type(self):
        count_bases_by_transposon_type(self.input_data, self.output_file)
        with open(self.output_file, 'r') as file:
            output = file.read()
            self.assertEqual(output, self.expected_output)

    def test_run_count_bases_by_transposon_type(self):
        args = argparse.Namespace(input=self.input_data, output=self.output_file)
        run_count_bases_by_transposon_type(args)
        with open(self.output_file, 'r') as file:
            output = file.read()
            self.assertEqual(output, self.expected_output)


if __name__ == '__main__':
    # Run the unit tests
    unittest.main(argv=[''], exit=False)
    
    # Run the production code
    #input_data = 'chr1\t12\t30\tTE1_family#Type1\nchr1\t15\t25\tTE2_family#Type2\n'
    #output_file = 'output.txt'
    parser = argparse.ArgumentParser(description='Count the number of bases covered by each transposon Type/family')

    parser.add_argument('--input', type=str, help='input file with transposon coordinates', required=True)
    parser.add_argument('--output', type=str, help='output file with transposon counts', required=True)

    args = parser.parse_args()

    run_count_bases_by_transposon_type(args)
