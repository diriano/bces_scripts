#!/usr/bin/perl
 
use strict;
use warnings;
use diagnostics;
 
if (!@ARGV){die '';};
 
open LOG, ">cleaning.log";
my $phred_bin='/data/Progs/phred_phrap_crossmatch/phred';
my $cross_match_bin='/data/Progs/phred_phrap_crossmatch/cross_match';
my $vectordb='/data/DBs/UniVec/UniVec';
 

`$phred_bin -id $ARGV[0] -sd $ARGV[0] -qd $ARGV[0] -pd $ARGV[0] -trim_alt '' -trim_fasta`;
print "$phred_bin -id $ARGV[0] -sd $ARGV[0] -qd $ARGV[0] -pd $ARGV[0] -trim_alt '' -trim_fasta";
opendir(DIR, $ARGV[0]);
my @files=readdir(DIR);
foreach my $file(@files){
 next if $file =~/^\.+$/;
 if ($file=~/\.qual$/){
  my $newname=$file;
  $newname=~s/\.qual/\.seq\.qual/;
  rename  "$ARGV[0]/$file", "$ARGV[0]/$newname";
  print "PHRED: $file $newname\n";
 }
}
foreach my $file(@files){
 print $file."\n";
 if($file=~/\.seq$/){
  print "$file\n\n";
  if (!-r "$ARGV[0]/$file"){
   print LOG "File $ARGV[0]/$file does not exists\n";
   next;
  }
  print STDERR "\t$file \n";
  my $cross_match_summary_out="$ARGV[0]/$file.cross_match_summary.txt";
  my $cross_match_sequence_out= "$ARGV[0]/$file.screen";
  my $trimmedseq_file="$ARGV[0]/$file.trimmed";
  my $seq_length_cmd="infoseq $ARGV[0]/$file -only -length -auto -noheading";
  my $seq_length=`$seq_length_cmd`;
  chomp $seq_length;
  if ($seq_length<1){print LOG "No bases in file: $ARGV[0]/$file\n"; next;}
  my $mask_vector_cmd="$cross_match_bin -minmatch 10 -minscore 20 $ARGV[0]/$file $vectordb -screen > $cross_match_summary_out 2>/dev/null";
  my @crossmatch_out=`$mask_vector_cmd`;
  print "$mask_vector_cmd\n";
  my $trimseq_cmd="trimseq $cross_match_sequence_out -outseq $trimmedseq_file -auto";
  print "$trimseq_cmd\n";
  `$trimseq_cmd`;
 }
}
