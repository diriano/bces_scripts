#!env python3

import gzip
from Bio import SeqIO
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("infile", type=str,
                    help="provide the name of the fastq input file")
parser.add_argument("ids", type=str,
                    help="provide a file with the list of sequence identifiers to retrieve")
parser.add_argument("outfile", type=str,
                    help="provide a name file for output selected reads in fastq format")

args=parser.parse_args()

idsList=[]
with open(args.ids, "r") as idsHandle:
 for line in idsHandle:
  line = line.rstrip()
  idsList.append(line)

print(args.infile)
with gzip.open(args.infile, "rt") as handle, open(args.outfile, "w") as outputHandle:
    for record in SeqIO.parse(handle, "fastq"):
     print(record.id)
     if record.id in idsList:
      SeqIO.write(record, outputHandle, 'fastq')
      print(f'{record.id}')
