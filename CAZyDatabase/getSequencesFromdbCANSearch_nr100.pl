#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);
use File::Basename;
use Bio::DB::Fasta;
use Bio::SeqIO;

#Global variables
my $family='all';
my $seqdb_base='';
my $dbcanres_file='';
my %SeqsByFile;
my $dbCAN_seqsFolder='dbCAN_seqs';
my $version = 0.1;
my $help='';
my $license='';
my $debug=0;

###############################
## Get options
###############################

GetOptions
  (
   'dbcan|d=s'      => \$dbcanres_file,
   'family|f:s'     => \$family,
   'seqdb_base|s=s' => \$seqdb_base,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

###############################
## Get user options
###############################
if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $dbcanres_file){
 warn "FATAL: You must provide a file with the results of a dbCAN search (after using the dbCAN's hmmseach parser)\n";
 usage();
 exit 1;
}
if(!-d $seqdb_base){
 warn "FATAL no $seqdb_base directory. It must be the base path where the species specific protein fasta files are found\n";
 usage();
 exit 1;
}
if(-d $dbCAN_seqsFolder){
 die "Folder already exists. Delete before proceeding --".localtime()."\n"
}
###############################
## Process dbCAN results file
###############################

print STDOUT "Getting sequence IDs from $dbcanres_file -- ".localtime()."\n";
my %dbCan_Seqs;
open DBCAN, $dbcanres_file;
while(<DBCAN>){
 chomp;
 my ($fam, undef, $seqid, undef, undef, undef, undef, $start, $end)=split(/\t/);
 $fam=~s/\.hmm//;
 if ($family eq 'all'){
  push @{ $dbCan_Seqs{$fam}{$seqid} }, {'start' => $start, 'end' => $end};
 }
 else{
  if($fam eq $family){
   push @{ $dbCan_Seqs{$fam}{$seqid} }, {'start' => $start, 'end' => $end};
  }
 }
# print "$fam\t$seqid\n";
}

###############################
## Indexing database
###############################

my $seqdbgz_basename=$dbcanres_file;
$seqdbgz_basename=~s/\.dbCAN\.families\.out/.mod.faa.gz/;
my $seqdbgz_fullpath=$seqdb_base."/".$seqdbgz_basename;
if (!-s $seqdbgz_fullpath){
 die "Sequence database file $seqdbgz_fullpath does not exists . . .\n";
}
else{
 print STDOUT "Will be using $seqdbgz_fullpath as fasta database to extract sequences -- \nIndexing database -- ".localtime()."\n";
}
$seqdbgz_fullpath=~s/\/\//\//g;
my $seqdb_fullpath=$seqdbgz_fullpath;
$seqdb_fullpath=~s/\.gz$//;
gunzip $seqdbgz_fullpath => $seqdb_fullpath;

#my $seqinObj = Bio::SeqIO->new(
my $seqinObj = Bio::DB::Fasta->new($seqdb_fullpath);

###############################
## Extract selected sequences from Fasta file
############################### 

print STDOUT "Extracting selected sequences -- ".localtime()."\n";
foreach my $fam(keys %dbCan_Seqs){
 my ($filename,$path)=fileparse($dbcanres_file);
 my $outFile="Fam__".$fam.'__'.$filename;
 $outFile=~s/\.dbCAN\.families\.out/.faa/;
 $outFile=$path.$outFile;
 print "\t$outFile\t$path\n" if $debug>5;
 my $outFileObj = Bio::SeqIO->new(-file => ">$outFile" ,
                                  -format => 'FASTA');
 foreach my $id(keys %{$dbCan_Seqs{$fam}}){ 
  my $seqObj = $seqinObj->get_Seq_by_id($id);
  my $countDomaininSeq=1;
  foreach my $hit(@{$dbCan_Seqs{$fam}{$id}}){
   print "$id $fam ".$$hit{'start'}."\t".$$hit{'end'}."\n" if $debug>5;
   my $domainSeq=  $seqObj->subseq($$hit{'start'},$$hit{'end'});
   my $id=$seqObj->id;
   $id=$fam."_____".$id."_____count$countDomaininSeq";
   my $newSeqObj=Bio::PrimarySeq->new ( -seq => $domainSeq,
                                        -id  => $id); 
   $outFileObj->write_seq($newSeqObj);
   $countDomaininSeq++;
  }
 }
 my $outFilenr100=$outFile; 
 $outFilenr100=~s/.faa$/.nr100.faa/;
 `cd-hit -i $outFile -o $outFilenr100 -d 0 -c 1`;
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2016 Mauricio Ria<C3><B1>o Pach<C3><B3>n\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0  extracts protein sequences for dbCAN hists, and creates and nr100 dataset for the species

USAGE
    $0 

OPTIONS
    --dbcan,      -d     dbCAN results files (after running the HMMsearch parser). REQUIRED
    --family,     -f     Family of interest. use ALL to get the sequences for all
                          families, one files will be generate for each family.
                          Defaul = all                                             OPTIONAL
    --seqdb_base, -s     Path where species speciefic proteins multifasta files
    --help,       -h     This help.
    --license,    -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2016 Mauricio Ria<C3><B1>o Pach<C3><B3>n
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

