#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

my $path    ='';
my $license = '';
my $help    = '';
my $version = 0.1;
my %famFiles;
###############################
## Get options
###############################
GetOptions
  (
   'path|p=s'       => \$path,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

###############################
## Get user options
###############################
if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-d $path){
 warn "FATAL: You must provide an existing path where the Family files are located\n";
 usage();
 exit 1;
}

mkdir $path.'/ALGN';

opendir(DIR, $path) || die "can't opendir $path: $!";
while(my $file=readdir(DIR)) {
 if ($file=~/^Fam__([A-Z]+[0-9]+)__.+\.nr100\.faa$/){
  my $fam=$1;
  push @{$famFiles{$fam}},$file;
 }
}
closedir DIR;

foreach my $fam(keys %famFiles){
  my $outFamFile=$fam."___allSps.faa";
  $outFamFile=$path.'/ALGN/'.$outFamFile;
  open FAM, ">$outFamFile";
  foreach my $aafile(@{$famFiles{$fam}}){
   my $fln=$path."/".$aafile;
   open INFILE, "$fln";
   while(<INFILE>){
    print FAM $_;
   }
   close INFILE;
  }
  close FAM;
}
#foreach my $fam(keys %famPerSpecies){
# $outFamFile=$path1.$outFamFile;
# open FAM, ">$outFamFile";
# foreach my $file(keys %{$famPerSpecies{$fam}}){
#  open IN, $file;
#  while(<IN>){
#   print FAM $_;
#  }
#  close IN;
# }
# close FAM;
#}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2016 Mauricio Ria<C3><B1>o Pach<C3><B3>n\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0  Join fasta files for the same family. It expects a very specific filename pattern

USAGE
    $0 

OPTIONS
    --path,       -p     What where per family sequence files are locate. REQUIRED
    --help,       -h     This help.
    --license,    -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2016 Mauricio Ria<C3><B1>o Pach<C3><B3>n
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

