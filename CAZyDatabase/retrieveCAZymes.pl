#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;
use LWP::UserAgent;
use HTML::TableExtract;
use Data::Dumper;
use HTML::Entities;

my $version             ='0.1';
my $help                ='';
my $license             ='';
my $debug               =0;
my $family              ='';
my $category            ='';
my $outbase             ='';

my @allowed_GHs=("GH1","GH2","GH3","GH4","GH5","GH6","GH7","GH8","GH9","GH10","GH11","GH12","GH13","GH14","GH15","GH16","GH17","GH18","GH19","GH20","GH21","GH22","GH23","GH24","GH25","GH26","GH27","GH28","GH29","GH30","GH31","GH32","GH33","GH34","GH35","GH36","GH37","GH38","GH39","GH40","GH41","GH42","GH43","GH44","GH45","GH46","GH47","GH48","GH49","GH50","GH51","GH52","GH53","GH54","GH55","GH56","GH57","GH58","GH59","GH60","GH61","GH62","GH63","GH64","GH65","GH66","GH67","GH68","GH69","GH70","GH71","GH72","GH73","GH74","GH75","GH76","GH77","GH78","GH79","GH80","GH81","GH82","GH83","GH84","GH85","GH86","GH87","GH88","GH89","GH90","GH91","GH92","GH93","GH94","GH95","GH96","GH97","GH98","GH99","GH100","GH101","GH102","GH103","GH104","GH105","GH106","GH107","GH108","GH109","GH110","GH111","GH112","GH113","GH114","GH115","GH116","GH117","GH118","GH119","GH120","GH121","GH122","GH123","GH124","GH125","GH126","GH127","GH128","GH129","GH130","GH131","GH132","GH133","GH134","GH135");
my @allowed_AAs=("AA1","AA2","AA3","AA4","AA5","AA6","AA7","AA8","AA9","AA10","AA11","AA12","AA13");
my @allowed_CEs=("CE1","CE2","CE3","CE4","CE5","CE6","CE7","CE8","CE9","CE10","CE11","CE12","CE13","CE14","CE15","CE16");
my @allowed_CBMs=("CBM1","CBM2","CBM3","CBM4","CBM5","CBM6","CBM7","CBM8","CBM9","CBM10","CBM11","CBM12","CBM13","CBM14","CBM15","CBM16","CBM17","CBM18","CBM19","CBM20","CBM21","CBM22","CBM23","CBM24","CBM25","CBM26","CBM27","CBM28","CBM29","CBM30","CBM31","CBM32","CBM33","CBM34","CBM35","CBM36","CBM37","CBM38","CBM39","CBM40","CBM41","CBM42","CBM43","CBM44","CBM45","CBM46","CBM47","CBM48","CBM49","CBM50","CBM51","CBM52","CBM53","CBM54","CBM55","CBM56","CBM57","CBM58","CBM59","CBM60","CBM61","CBM62","CBM63","CBM64","CBM65","CBM66","CBM67","CBM68","CBM69","CBM70","CBM71");
my @allowed_PLs=("PL1","PL2","PL3","PL4","PL5","PL6","PL7","PL8","PL9","PL10","PL11","PL12","PL13","PL14","PL15","PL16","PL17","PL18","PL19","PL20","PL21","PL22","PL23");
my @allowed_GTs=("GT1","GT2","GT3","GT4","GT5","GT6","GT7","GT8","GT9","GT10","GT11","GT12","GT13","GT14","GT15","GT16","GT17","GT18","GT19","GT20","GT21","GT22","GT23","GT24","GT25","GT26","GT27","GT28","GT29","GT30","GT31","GT32","GT33","GT34","GT35","GT36","GT37","GT38","GT39","GT40","GT41","GT42","GT43","GT44","GT45","GT46","GT47","GT48","GT49","GT50","GT51","GT52","GT53","GT54","GT55","GT56","GT57","GT58","GT59","GT60","GT61","GT62","GT63","GT64","GT65","GT66","GT67","GT68","GT69","GT70","GT71","GT72","GT73","GT74","GT75","GT76","GT77","GT78","GT79","GT80","GT81","GT82","GT83","GT84","GT85","GT86","GT87","GT88","GT89","GT90","GT91","GT92","GT93","GT94","GT95","GT96","GT97","GT98");
my @allowed_families=(@allowed_GHs,@allowed_AAs,@allowed_CEs,@allowed_CBMs,@allowed_PLs,@allowed_GTs);

my $ua = LWP::UserAgent->new;

$ua->agent("CTBE-RetrieveCAZyInfo-PPB/0.1");
$ua->from("bce.ctbe\@gmail.com");
$ua->env_proxy;

##########################################
#Get options from the user
##########################################
GetOptions(
    'license|l'       => \$license,
    'help|h|?'        => \$help,
    'family|f=s'      => \$family,
    'category|c=s'    => \$category,
    'outbase=s'       => \$outbase,
    'debug=i'         => \$debug
);

############################
##Check input from user
#############################
if ($help){
 &usage();
 exit(1);
}
if ($license){
 &license();
 exit(1);
}
if (!$family){
 print STDERR "FATAL:  You must provide a family to search for in cazy.org.\n";
 &usage();
 exit(1);
}
if(!(uc($family) ~~ @allowed_families)){
 print "This is not an allowed family. Must be one of @allowed_families\n";
 &usage();
 exit(1);
}
if (!$category){
 print STDERR "FATAL:  You must provide a category.\n";
 &usage();
 exit(1);
}
if(lc($category) !~ /(all|archaea|bacteria|eukaryota|unclassified|characterized)/i){
 print STDERR "FATAL:  You must provide an allowed category.\nAllowed values for category are: all|archaea|bacteria|eukaryota|unclassified|characterized\n";
 &usage();
 exit(1);
}
if(!$outbase){
 print STDERR "FATAL:  You must provide a prefix that will be used to create output files\n";
 &usage();
 exit(1);

}
print "There are ".scalar(@allowed_families)." allowed families\n" if $debug >4;

my $html=getHTML($family,$category,'');
my ($uniprotIDs,$genbankIDs,$pages)=parseHTML($html,'0');
foreach my $page(@$pages){
 next if $page eq 'NA';
 my $html2=getHTML($family,$category,$page);
 my ($uniprotIDs2,$genbankIDs2,$pages2)=parseHTML($html2,$page);
 push @$uniprotIDs,@$uniprotIDs2;
 push @$genbankIDs,@$genbankIDs2;
}

my %sawup;
undef %sawup;
my @uniqueUniprotIDs = grep(!$sawup{$_}++, @$uniprotIDs);

my %sawgb;
undef %sawgb;
my @uniqueGenbankIDs = grep(!$sawgb{$_}++, @$genbankIDs);

my $genbankOUT=$outbase."_genbank_".$family."_".$category;
my $uniprotOUT=$outbase."_uniprot_".$family."_".$category;

open GOUT, ">$genbankOUT";
foreach my $u(@uniqueGenbankIDs){ next if $u eq '';next if $u eq 'GenBank';print GOUT "$u\n"}
close GOUT;

open UOUT, ">$uniprotOUT";
foreach my $u(@uniqueUniprotIDs){ next if $u eq '';next if $u eq 'Uniprot';print UOUT "$u\n"}
close UOUT;

sub getHTML{
 my ($f,$c,$p)=@_;
 my $URL="http://www.cazy.org/".uc($f)."_".lc($c).".html?debut_PRINC=".$p;
 print $URL."\n" if $debug > 4;
 my $response = $ua->get($URL);
 if ($response->is_success) {
  print $response->decoded_content if $debug > 10;
  return $response->decoded_content;
 }
 else {
     die $response->status_line;
 }
}

sub parseHTML{
 my $html=shift;
 my $page=shift;
 my @u=();
 my @g=();
 my $htmlpaginationparser=HTMLParserClassPages->new;
 $htmlpaginationparser->parse($html);
 my @pages = defined($HTMLParserClassPages::getpages) ? split(/ /,$HTMLParserClassPages::getpages) : "NA"; #This return the number of additional pages that must be retrieved from CAZy
 my $te = HTML::TableExtract->new( attribs => { id => 'pos_onglet' } );
 $te->parse($html);
 my $table = $te->first_table_found;
 foreach my $row ($table->rows){
  my $uniprotID= defined(encode_entities($$row[4])) ? encode_entities($$row[4]) : '';
  my $genbankID= defined(encode_entities($$row[3])) ? encode_entities($$row[3]) : '';
  $uniprotID=~s/&nbsp;//g;
  $uniprotID=~s/ //g;
  my @uniprotID= $uniprotID=~/\n/ ? split(/\n/,$uniprotID) : $uniprotID;
  $genbankID=~s/&nbsp;//g;
  $genbankID=~s/ //g;
  my @genbankID= $genbankID=~/\n/ ? split(/\n/,$genbankID) : $genbankID;
  push @u, @uniprotID;
  push @g, @genbankID;
 }
 return (\@u,\@g,\@pages);
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2014 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0  retrieves UniProtID for given families in CAZy.org

USAGE
    $0 --family GH1 --category characterized --outbase outFile

OPTIONS
    --family   -f        CAZy family                          REQUIRED
    --category -c        Category. Any of hte following:      REQUIRED
                          all, archaea, bacteria, eukaryota, unclassified or characterized
    --outbase            Prefix to create output filenames    REQUIRED
    --debug              default 0                            OPTIONAL
    --help     -h        This help.
    --license  -l        License.

EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2014 Diego Mauricio Riaño Pachón
http://bce.bioetanol.cnpem.br
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}


package HTMLParserClassPages;
use base qw(HTML::Parser);
our ($getpages);
sub start { 
 my ($self, $tagname, $attr, $attrseq, $origtext) = @_;
 if ($tagname eq 'a') {
  if($attr->{ class } ){
   if($attr->{ class } eq 'lien_pagination' && $attr->{ rel } eq 'nofollow'){
    if($attr->{ href }=~/^.+_.+\.html\?debut_PRINC=(\d+)#pagination_PRINC/){
     $getpages .= $1.' ';
    }
    elsif($attr->{ href }=~/^.+_.+\.html#pagination_PRINC/){}
    else{
     die "The link does not look like you imagined. Check: ". $attr->{ href }."\n";
    }
   }
  }
 }
}
