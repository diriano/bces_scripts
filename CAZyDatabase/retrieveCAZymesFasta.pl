#!/usr/bin/perl --

use strict;
use warnings;
use Getopt::Long;
use LWP::UserAgent;
use LWP::Simple;

my $version             ='0.2';
my $help                ='';
my $license             ='';
my $debug               =0;
my $log                 ='';
my $database            ='';
my $outbase             ='';
my $outfile             ='';
my $infile              ='';
my $url                 ='';
my $id                  ='';
my @allowed_DBs=("genbank","uniprot");

##########################################
#Get options from the user
##########################################

GetOptions(
    'license|l'       => \$license,
    'help|h|?'        => \$help,
    'infile|in=s'     => \$infile,
    'database|db=s'   => \$database,
    'outbase=s'       => \$outbase,
    'debug=i'         => \$debug
);

############################
##Check input from user
#############################

if ($help){
 &usage();
 exit(1);
}
if ($license){
 &license();
 exit(1);
}
if(!$database){
 print STDERR "FATAL: You must provide a database to retrieve FASTA sequences\n";
 &usage();
 exit(1);
}
if(!(lc($database) ~~ @allowed_DBs)){
 print lc($database)."\n";
 print STDERR "FATAL: You must provide an allowed database (GenBank OR Uniprot)";
 &usage();
 exit(1);
}
if(!$infile){
 print STDERR "FATAL: You must provide an input file with IDs\n";
 &usage();
 exit(1);
}
if(!$outbase){
 print STDERR "FATAL: You must provide a prefix that will be used to create output files\n";
 &usage();
 exit(1);
}

my $ua = LWP::UserAgent->new;

$ua->agent("CTBE-RetrieveFasta-PPB/0.2");
$ua->from("bce.ctbe\@gmail.com");
$ua->env_proxy;

if(lc($database) eq 'uniprot') {
 $outfile = $outbase."_uniprot.fasta";
 print "Your outfile is: ".$outfile if $debug > 2;
}
if(lc($database) eq 'genbank') {
 $outfile = $outbase."_genbank.fasta";
 print "Your outfile is ".$outfile if $debug > 2;
}

if(lc($database) eq 'uniprot') {
  open(INFILE,$infile);
   while(<INFILE>) {
   chomp;
   if(/^([0-9A-Z][0-9A-Z][0-9A-Z][0-9A-Z][0-9A-Z][0-9A-Z])$/) {
    $id=$1;
    my $page=getHTMLuniprot($id);
    if($page =~ /^>/) {
     open(OUTFILE,">>$outfile");
     print OUTFILE "$page";
     close(OUTFILE);
     sleep 1;
    }
   }
   else {
    warn "The ID "."$_"." is not as expected!!\n";
    #die "The ID "."$_"." is not as expected!!\n";
   }
  }
 close(INFILE);
}
elsif(lc($database) eq 'genbank') {
 my %groupIDs;
 my $index=0;
 my $countIds=0;
 open(INFILE,$infile);
 while(<INFILE>) {
  chomp;
  if(/^([A-Z0-9]+\.[0-9])$/ or /^([A-Z0-9]+_[A-Z0-9]+\.[0-9])$/ or /^[0-9A-Z]+$/) {
   $id=$1;
   $countIds++;
#   print "$index $id\n";
   $groupIDs{$index}{$id}=1;
   if ($countIds % 200 == 0){ #If more than 200 IDs are eFetch, the HTTP method must be POST, I am keeping it at 200
    $index++
   }
  }
  else {
   warn "The ID "."$_"." is not as expected!!\n";
   #die "The ID "."$_"." is not as expected!!\n";
  }
 }
 close(INFILE);
 open(OUTFILE,">$outfile");
 foreach my $inx(keys %groupIDs){
  sleep 2;
  my $ids=join(',',keys %{$groupIDs{$inx}});
  print "$inx $ids\n";
  my $seq=getHTMLgenbank($ids);
  if($seq =~ /^>/) {
   print OUTFILE "$seq";
  }
 }
 close(OUTFILE);
}

sub getHTMLuniprot{
 my $s=shift;
 my $URL="http://www.uniprot.org/uniprot/".$s.".fasta";
 print $URL."\n" if $debug > 2;
 my $response = $ua->get($URL);
 if ($response->is_success) {
  print $response->decoded_content if $debug > 2;
  return $response->decoded_content;
 }
 else {
  $log = $infile."CAZy_FASTA.log";
  open(LOGFILE,">>$log");
  print LOGFILE "For ".$s.", we had the following HTML response: ".$response->status_line."\n";
  #print LOGFILE $response->status_line."\n";
  close(LOGFILE);
  #warn $response->status_line;
  #die $response->status_line;
 }
}

sub getHTMLgenbank {
 my $s=shift;
 my $URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=protein&term=$s&usehistory=y";
 print $URL."\n" if $debug > 2;
 my $output = get($URL);
 my $web = $1 if ($output =~ /<WebEnv>(\S+)<\/WebEnv>/);
 my $key = $1 if ($output =~ /<QueryKey>(\d+)<\/QueryKey>/);
 $url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&query_key=".$key."&WebEnv="."$web"."&rettype=fasta&retmode=text";
 my $response = $ua->get($url);
 if ($response->is_success) {
  print $response->decoded_content if $debug > 2;
  return $response->decoded_content;
 }
 else {
  $log = $infile."CAZy_FASTA.log";
  open(LOGFILE,">>$log");
  print LOGFILE "For ".$s.", we had the following HTML response: "."$response->status_line"."\n";
  #print LOGFILE $response->status_line."\n";
  close(LOGFILE);
  #warn $response->status_line;
  #die $response->status_line;
 }
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2014 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0  retrieves protein FASTA sequences from 'UniProt' OR 'NCBI' database.

USAGE
    $0 --database genbank --infile list.txt --outbase GH10.fasta

OPTIONS
    --infile             Input IDs                            REQUIRED
                           Allowed: Uniprot IDs ('--db uniprot') or GenBank IDs ('--db genbank').
    --database -db       Database                             REQUIRED
    --outbase            Prefix to create output filenames    REQUIRED
    --debug              default 0                            OPTIONAL
    --help     -h        This help.
    --license  -l        License.

EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2014 Diego Mauricio Riaño Pachón
http://bce.bioetanol.cnpem.br
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}
