#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Getopt::Long;
use Bio::SeqIO;
use Bio::DB::Fasta;

#Set global vars
my $version = 0.1;
my $trinity_file= '';
my $help='';
my $license='';
my $minCoverage=0.5;
my %longestIsoform;
#Set options
GetOptions
  (
   'trinity|t=s'    => \$trinity_file,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

#check input form user
if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $trinity_file){
 warn "FATAL: You must provide a Trinity.fasta file\n";
 usage();
 exit 1;
}
if($trinity_file ne 'Trinity.fasta'){
 warn "FATAL: You must provide a Trinity.fasta file, with exactly that name\n";
 usage();
 exit 1;
}

#process input
my $seqioObj = Bio::SeqIO->new(-file => $trinity_file);
while(my $seqObj   = $seqioObj->next_seq){
 my @f=split(/_/,$seqObj->id);
 my $gene=join("_",@f[0..3]);
 if($longestIsoform{$gene}){
  if($longestIsoform{$gene}{'longestisoform_length'} < $seqObj->length){
   $longestIsoform{$gene}{'longestisoform_id'}=$seqObj->id;
   $longestIsoform{$gene}{'longestisoform_length'}=$seqObj->length;
  }
 }
 else{
  $longestIsoform{$gene}{'longestisoform_id'}=$seqObj->id;
  $longestIsoform{$gene}{'longestisoform_length'}=$seqObj->length;
 }
}

my $outfile=$trinity_file;
$outfile=~s/\.fasta/.longestisoform.fasta/;

my $seqOutObj = Bio::SeqIO->new(
                              -file   => ">$outfile",
                              -format => "fasta",
                              );

my $db=Bio::DB::Fasta->new($trinity_file);
foreach my $gene(keys %longestIsoform){
 my $seqObj     = $db->get_Seq_by_id($longestIsoform{$gene}{'longestisoform_id'}); 
 $seqOutObj->write_seq($seqObj);  
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2016 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   Extract the longest isoform from each gene using a Trinity.fasta as input.

USAGE
    $0 --trinity Trinity.fasta

OPTIONS
    --trinity   -t     Trinity.fasta file.            REQUIRED
    --help,     -h     This help.
    --license,  -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2016 Diego Mauricio Riaño Pachón
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}


