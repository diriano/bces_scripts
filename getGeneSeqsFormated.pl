#!/usr/bin/perl

use strict;
use warnings;
use List::Util qw(max);
use Bio::DB::Fasta;
 #usage: script.pl genome.gff3 genome.fasta. Make usre genome.gff3 is dorted and has the explicit intro features. If not use GenomeTools to fix your gff3
die if !@ARGV;
my %seqs;
my %transc2gene;
my ($transcriptID,$geneID)=('','');
my %strOrder;
my $strOrderCount=0;
my %tbl;
my $tblOutFile='geneExonIntron.tbl';
my $geneSimpleSeqsOutFile='gene.simple.seqs.fasta';
my $geneFormatedSeqsOutFile='gene.formated.seqs.fasta';
open GENESIMPLEOUT, ">$geneSimpleSeqsOutFile";
open SEQOUT, ">$geneFormatedSeqsOutFile";
open TBLOUT, ">$tblOutFile";

my $genomeDB=Bio::DB::Fasta->new($ARGV[1]);
open GFF3, $ARGV[0];
while(<GFF3>){
 chomp;
 next if /^#/;
 my @fields=split(/\t/);
 if($fields[2] eq 'gene'){ 
  $transcriptID='';
  $geneID='';
 }
 elsif($fields[2] eq 'mRNA'){
  $strOrderCount=0;
  my @f2=split(/;/,$fields[-1]);
  foreach my $f(@f2){ 
   if ($f=~/^ID=/){
    $transcriptID=$f;
    $transcriptID=~s/ID=//;
   }
   elsif($f=~/Parent=/){
    $geneID=$f;
    $geneID=~s/Parent=//;
   }
  }
#  print "$transcriptID\t$geneID\n";
  $transc2gene{$transcriptID}=$geneID;
  my $seqstrg='';
  if($fields[6] eq '+'){
   $seqstrg=$genomeDB->seq($fields[0], $fields[3] => $fields[4]);
   print GENESIMPLEOUT ">$transcriptID $geneID Strand:$fields[6] Start:$fields[3] End:$fields[4]\n$seqstrg\n";
  }
  else{
   $seqstrg=$genomeDB->seq($fields[0], $fields[4] => $fields[3]);
   print GENESIMPLEOUT ">$transcriptID $geneID Strand:$fields[6] Start:$fields[3] End:$fields[4]\n$seqstrg\n";
  }
  $seqs{$transcriptID}=$seqstrg;
 }
 elsif($fields[2] eq 'exon' || $fields[2] eq 'intron'){
  my $transcriptID2='';
  #print $_."\n";
  my @f2=split(/;/,$fields[-1]);
  foreach my $f(@f2){
   if($f=~/Parent=/){
    $transcriptID2=$f;
    $transcriptID2=~s/Parent=//;
#    print $transcriptID2."\n";
   }
  }
  if($fields[2] eq 'exon'){
   #print "$transcriptID2\t".uc($seqstrg)."\n";
   my $string="$transc2gene{$transcriptID2}\t$transcriptID2\t$fields[6]\t$strOrderCount\tE:".$fields[3]."-".$fields[4];
   $tbl{$transcriptID2}{'geneID'}=$transc2gene{$transcriptID2};
   $tbl{$transcriptID2}{'strand'}=$fields[6];
   $tbl{$transcriptID2}{'order'}{$strOrderCount}="E:".$fields[3]."-".$fields[4];
   $strOrderCount++;
  }
  elsif($fields[2] eq 'intron'){ 
   #print "$transcriptID2\t".lc($seqstrg)."\n";
   my $string="$transc2gene{$transcriptID2}\t$transcriptID2\t$fields[6]\t$strOrderCount\tI:".$fields[3]."-".$fields[4];
   $tbl{$transcriptID2}{'geneID'}=$transc2gene{$transcriptID2};
   $tbl{$transcriptID2}{'strand'}=$fields[6];
   $tbl{$transcriptID2}{'order'}{$strOrderCount}="I:".$fields[3]."-".$fields[4];
   $strOrderCount++;
  }
 }
}
close GFF3;
close GENESIMPLEOUT;

foreach my $t(keys %tbl){
 my $formatedSeq='';
 if($tbl{$t}{'strand'} eq '+'){
  for (my $i=0;$i<=max(keys %{$tbl{$t}{'order'}});$i++){
   my ($type,$start,$end)=split(/[:-]/,$tbl{$t}{'order'}{$i});
   my $lengthPreviousCummulative=getLengthPreviousCummulative($t,$i);
   my $lengthFragment=$end-$start+1;
   my $newEnd=$lengthFragment+$lengthPreviousCummulative;
   my $newStart=$newEnd-$lengthFragment+1;
   my $newStart0=$newStart-1;
   my $newEnd0=$newEnd-1;
   my $fragSeq=$type eq 'I'?lc(substr($seqs{$t},$newStart0,$lengthFragment)):uc(substr($seqs{$t},$newStart0,$lengthFragment));
   print TBLOUT $t."\t".$tbl{$t}{'geneID'}."\t".$tbl{$t}{'strand'}."\t$i\t".$tbl{$t}{'order'}{$i}."\t".$lengthPreviousCummulative."\tn$type:".$newStart."($newStart0)-".$newEnd."($newEnd0)\t".length($seqs{$t})."\t".length($fragSeq)."\t".$fragSeq."\n";
   $formatedSeq.=$fragSeq;
  }
 }
 elsif($tbl{$t}{'strand'} eq '-'){
  for (my $j=max(keys %{$tbl{$t}{'order'}});$j>=0;$j--){
   my ($type,$start,$end)=split(/[:-]/,$tbl{$t}{'order'}{$j});
   my $lengthPreviousCummulative=getLengthPreviousCummulativeMinusStrand($t,$j);
   my $lengthFragment=$end-$start+1;
   my $newEnd=$lengthFragment+$lengthPreviousCummulative;
   my $newStart=$newEnd-$lengthFragment+1;
   my $newStart0=$newStart-1;
   my $newEnd0=$newEnd-1;
   my $fragSeq=$type eq 'I'?lc(substr($seqs{$t},$newStart0,$lengthFragment)):uc(substr($seqs{$t},$newStart0,$lengthFragment));
   print TBLOUT $t."\t".$tbl{$t}{'geneID'}."\t".$tbl{$t}{'strand'}."\t$j\t".$tbl{$t}{'order'}{$j}."\t".$lengthPreviousCummulative."\tn$type:".$newStart."($newStart0)-".$newEnd."($newEnd0)\t".length($seqs{$t})."\t".length($fragSeq)."\t".$fragSeq."\n";
   $formatedSeq.=$fragSeq;
  }
 }
 print SEQOUT ">$t $tbl{$t}{'geneID'} Strand:$tbl{$t}{'strand'}\n$formatedSeq\n";
}
close TBLOUT;
close SEQOUT;

sub getLengthPreviousCummulativeMinusStrand{
 my $transcript=shift;
 my $order=shift;
 my $cummLength=0;
 for (my $i=max(keys %{$tbl{$transcript}{'order'}});$i>$order;$i--){
  my (undef,$start,$end)=split(/[:-]/,$tbl{$transcript}{'order'}{$i});
  my $length=$end-$start+1;
  $cummLength=$cummLength+$length;
  #print $i."\t$order\t$transcript\t$start\t$end\t$length\t$cummLength\n";
 }
 return($cummLength);
}

sub getLengthPreviousCummulative{
 my $transcript=shift;
 my $order=shift;
 my $cummLength=0;
 for (my $i=0;$i<$order;$i++){
  my (undef,$start,$end)=split(/[:-]/,$tbl{$transcript}{'order'}{$i});
  my $length=$end-$start+1;
  $cummLength=$cummLength+$length;
  #print $i."\t$order\t$transcript\t$start\t$end\t$length\t$cummLength\n";
 }
 return($cummLength);
}
