#!/usr/bin/perl

###############################################################
###############################################################
## Script: getPromotersAndIntergenicRegios.pl                ##
##  Takes a GFF3 file and extract upstream sequences from    ##
##   genes, and intergenic regions.                          ##
##  Length of estracted sequence can be specificed by        ##
##   the user. Default is 1000bp                             ##
## This script assumes that your original genome sequence    ##
##  DOES NOT have the ambiguous base W                       ##
## IF IT DOES HAVE IT, REFRAIN FROM USING THIS SCRIPT        ##  
###############################################################
###############################################################
use strict;
use warnings;
use diagnostics;
use Bio::DB::Fasta;
use Bio::SeqIO;

use Getopt::Long;

my $version="0.1";
my $fastafile='';
my $gff3file='';
my $baseout='';
my $promoterlength=1500;
my $license='';
my $help='';

#Get input from user

GetOptions(
    'license|l'            => \$license,
    'help|h|?'             => \$help,
    'fastafile|f=s'        => \$fastafile,
    'gff3file|g=s'         => \$gff3file,
    'promoterlength|p=i'   => \$promoterlength,
    'baseout|b=s'          => \$baseout,
);

#Check input from user

if($help){
 &usage;
 exit 1
}
if($license){
 &license;
 exit 1
}
if(!-s $fastafile){
 print STDERR "\n\tFATAL:  You must provide a multifasta file.\n\n";
 &usage;
 exit 0;
}
if(!-s $gff3file){
 print STDERR "\n\tFATAL:  You must provide a GFF3 file.\n\n";
 &usage;
 exit 0;
}
if(!$baseout){
 print STDERR "\n\tFATAL:  You must provide a base name to create the output files.\n\n";
 &usage;
 exit 0;
}

my $maskedFileOut=$baseout."_masked.fasta";
my $intergenicFastaFileOut=$baseout."_intergenic.fasta";
my $promoterFastaFileOut=$baseout."_promoters_".$promoterlength."bp.fasta";
my $geneCoords=getGeneCoordinates($gff3file);
print STDERR "Found ".scalar(keys(%$geneCoords))." contigs with annotated genes\n";
if(!-s $maskedFileOut){
 print STDERR "Masking genome file ".localtime()."\n";
 maskGenes($fastafile,$geneCoords,$maskedFileOut);
}
if(!-s $intergenicFastaFileOut){
 print STDERR "Extracting intergenic regions ".localtime()."\n";
 getIntergenicRegions($maskedFileOut,$intergenicFastaFileOut);
}
if(!-s $promoterFastaFileOut){
 print STDERR "Extracting promoters, $promoterlength bp upstream of the start of the gene ".localtime()."\n";
 getPromoters($geneCoords,$maskedFileOut,$promoterlength,$promoterFastaFileOut);
}

unlink $maskedFileOut;

sub getPromoters{
 my $g_ref=shift;
 my $maskedGenome=shift;
 my $length=shift;
 my $promoterFastaFile=shift;
 my $promoterFastaObj = new Bio::SeqIO(-file   => ">$promoterFastaFile",
                                       -format => 'fasta');
 my $dbinx=indexdb($maskedGenome);

 foreach my $chr(keys %$g_ref){
  my $seqObj=$dbinx->get_Seq_by_id($chr);
  foreach my $gene(keys%{$$g_ref{$chr}}){
   my $promoterSeq='';
   my $desc='';
   my $promoterStart=0;
   my $promoterEnd=0;
   if ($$g_ref{$chr}{$gene}{'strand'} eq '+'){
    $promoterEnd=$$g_ref{$chr}{$gene}{'start'}-1;
    next if $promoterEnd==0;
    $promoterStart=$promoterEnd<$promoterlength?1:$promoterEnd-$promoterlength+1;
    $promoterSeq=$seqObj->subseq($promoterStart => $promoterEnd);
   }
   elsif($$g_ref{$chr}{$gene}{'strand'} eq '-'){
    $promoterStart=$$g_ref{$chr}{$gene}{'end'}+1;
    $promoterEnd=$promoterStart+$promoterlength>$seqObj->length?$seqObj->length:$promoterStart+$promoterlength-1;
    $promoterSeq=$dbinx->seq("$chr:$promoterEnd,$promoterStart");
   }
   if($promoterSeq ne ''){
    $promoterSeq=~s/^W+//i; 
    $promoterSeq=~s/^([ACGT]+W+)+//i; 
    next if $promoterSeq eq '';
    $desc="PromoterCoords:$promoterStart - $promoterEnd Strand:$$g_ref{$chr}{$gene}{'strand'} GeneCoords:$$g_ref{$chr}{$gene}{'start'} - $$g_ref{$chr}{$gene}{'end'} Trimmed if gene in upstream region";
    #print $chr."\t$gene\t$desc\n";
    my $promoterSeqObj=Bio::Seq->new(-seq        => $promoterSeq,
                                     -display_id => $gene,
                                     -description=> $desc);
    $promoterFastaObj->write_seq($promoterSeqObj);
   }
  }
 }
}

sub getIntergenicRegions{
 my $fasta=shift;  
 my $intergenicFastaFile=shift;
 my $intergenicFastaObj = new Bio::SeqIO(-file   => ">$intergenicFastaFile",
                                         -format => 'fasta');
 my $dbinx=indexdb($fasta);
 foreach my $id($dbinx->get_all_primary_ids){
  my $seqObj=$dbinx->get_Seq_by_id($id);
  my $seq=$seqObj->seq;
  my @inter=split(/W+/,$seq);
  my $count=1;
  foreach my $intergenicRegion(@inter){
   next if length($intergenicRegion) < 100;
   my $intergenicId=$id."_intergenic_".$count;
   $intergenicFastaObj->write_seq(Bio::Seq->new(-seq        => $intergenicRegion,
                                                -display_id => $intergenicId,
                                                -description=> 'INTERGENIC'));
   $count++;
  }
 } 
}
##################################################################
## maskGenes
##  In order to ease the extraction of the intergenics regions
## I decided to mask all the genic regions with Xs, with this
## then I could just split on runs of Xs, and the resultsing seq
## are the intergenic regions.
##  This will also help to extract the "promoters". Understanding
## promoter as the upstream sequence from a gene, up to the end
## of the upstream gene or Nbp, whatever happens first. With the
## masked gene sequence, it is just a matter of extracting Nbp
# upstream of the gene, and then trim the resulting sequence, 
# removing runs of Xs from the beginning
##################################################################
sub maskGenes{
 my $fasta=shift;
 my $g_ref=shift;
 my $maskedFile=shift;
 my $maskchar='W';
 my $dbinx=indexdb($fasta);
 my $maskedFastaObj = new Bio::SeqIO(-file   => ">$maskedFile",
  			             -format => 'fasta');
 foreach my $id($dbinx->get_all_primary_ids){
  my $seqObj=$dbinx->get_Seq_by_id($id);
  my $seq=$seqObj->seq;
  if($$g_ref{$id}){
   foreach my $gene(keys %{$$g_ref{$id}}){
    my $offset=$$g_ref{$id}{$gene}{'start'}-1;
    my $len=($$g_ref{$id}{$gene}{'end'}-$$g_ref{$id}{$gene}{'start'})+1;
    substr($seq, $offset,$len, $maskchar x $len);
   }
   $maskedFastaObj->write_seq(Bio::Seq->new(-seq        => $seq,
				            -display_id => $seqObj->display_id,
				            -description=> 'MASKED'));
  }
  else{
   $maskedFastaObj->write_seq(Bio::Seq->new(-seq        => $seq,
			            	    -display_id => $seqObj->display_id,
				            -description=> 'MASKED NO GENES'));
  }
 }
}

sub indexdb{
 my $file=shift;
# my $reinx=1;
 my $reinx=0;
 my $dbinx = Bio::DB::Fasta->new("$file", -reindex=>$reinx);
}

sub getGeneCoordinates{
 my %g;
 my $gff3=shift;
 open GFF3, $gff3;
 while(<GFF3>){
  chomp;
  next if /^#/;
  next if /^$/;
  my @fields=split(/\t/);
  if ($fields[2] eq 'gene'){
   my ($id,undef)=split(/;/,$fields[-1]);
   $id=~s/ID=//;
   $g{$fields[0]}{$id}{'start'}=$fields[3];
   $g{$fields[0]}{$id}{'end'}=$fields[4];
   $g{$fields[0]}{$id}{'strand'}=$fields[6];
  }
 }
 return \%g;
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2017 Diego Mauricio Riano Pachon\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   extracts promoter regions and intergenic regions from a GFF3 and a genome fasta file.

USAGE
    $0 --fastafile file.fasta --promoterlength 1500 --gff3file file.gff3 --baseout output_base

OPTIONS
    --fastafile      -f    Multifasta file.                       REQUIRED
    --promoterlength -m    Length of upstream (promoter) region.
                           Default 1500.                          OPTIONAL
    --gff3file       -g    GFF3 file.                             REQUIRED
    --baseout        -b    Basenae to generate output files.      REQUIRED
    --help      -h    This help.
    --license   -l    License.

EOF
}
sub license{
    print STDERR <<EOF;

Copyright (C) 2017 Diego Mauricio Riano Pachon
e-mail: diriano\@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}
