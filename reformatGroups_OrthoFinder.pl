#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

my $version="1.0";
my $license='';
my $help='';
my $infile='';

my @labels=();
my %groups;
my %orthogroups;
my %geneBySp;

#Get input from user
#
GetOptions(
 'license|l'   => \$license,
 'help|h|?'    => \$help,
 'infile|i=s'  => \$infile
);

if($help){
 &usage;
 exit 1
}
if($license){
 &license;
 exit 1
}
if(!-s $infile){
 print STDERR"\n\tFATAL: You must provide an infile, the Orthogroups.tsv file form an OrthoFinder run.\n\n";
 &usage;
 exit 0;
}

open FILE, $infile or die "Cannot open $infile";
while (<FILE>){
 chomp;
 s///g;
 if (/^Orthogroup\t/){
  @labels=split(/\t/);
 # print "@labels\n";
 }
 else{
  my @fields=split(/\t/);
  for (my $i=1;$i<@fields;$i++){
   if($fields[$i] ne ''){
    $groups{$labels[$i]}{$fields[0]}=1;
   }
   foreach my $set ($fields[$i]){
    my @ids=split(/,/,$set);
    foreach my $id(@ids){
     $id=~s/ //g;
#     print "$labels[$i]|$id\t";
     $orthogroups{$fields[0]}{$labels[$i]}{$id}=1;
     $geneBySp{$labels[$i]}{$id}=1;
    }
   }
  }
#  print "\n";
 }
}
close FILE;

foreach my $sp(keys %groups){
 my $outfile='Orthogroups.'.$sp.'.txt';
 open OUTFILE,">$outfile";
 foreach my $ogr(keys %{$groups{$sp}}){
  print OUTFILE "$ogr\n";
 }
 close OUTFILE;
}


open OUTTBL, ">Orthogroups.tbl";
foreach my $og( keys %orthogroups){
 foreach my $sp (keys %{$orthogroups{$og}}){
  foreach my $gene (keys %{$orthogroups{$og}{$sp}}){
   print OUTTBL "$og\t$sp\t$gene\n";
  }
 }
}
close OUTTBL;
#foreach my $sp(keys %geneBySp){
# print STDERR "$sp\t".scalar(keys %{$geneBySp{$sp}})."\n";
#}
#

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2020 Diego Mauricio Riano Pachon\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0  reformats Orthogroups.tsv file

USAGE
    $0 --infile Orthogroups.tsv

OPTIONS
    --infile      -i    Orthogroups.tsv file   REQUIRED
    --help        -h    This help.
    --license     -l    License.

EOF
}
sub license{
    print STDERR <<EOF;

Copyright (C) 2020 Diego Mauricio Riaño Pach<C3>
http://diriano.github.io/
e-mail: diego.riano\@icena.usp.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

