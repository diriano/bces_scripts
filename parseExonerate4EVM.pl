#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Getopt::Long;

#Set global vars
my $version = 0.1;
my $exonerate_file= '';
my $help='';
my $license='';
my $debug = 0;
##Set options
GetOptions
  (
   'exonerate|e=s'   => \$exonerate_file,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

#Check input from user
if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $exonerate_file){
 warn "FATAL: You must provide a file with the Exonerate (gft format)\n";
 usage();
 exit 1;
}

my $target='';
my $count=0;
open IN, $exonerate_file;
while(<IN>){
 chomp;
 next if(/^Command/);
 next if(/^Hostname/);
 next if(/^##/);
 next if(/^# seqname source/);
 next if(/^#$/);
 my @fields=split(/\t/);
 if(/^# --- START OF GFF DUMP ---$/){
  print $_."\n" if $debug > 5;
  $target='';
  $count++;
 }
#  print $_."\n";
# }
 elsif(/^# --- END OF GFF DUMP ---$/){
  print $_."\n" if $debug > 5;
 }
 else{
  if($fields[2] eq 'gene'){
   print $_."\n" if $debug > 5;
   my @att=split(/;/,$fields[-1]);
   die if ($att[1] !~ /^ sequence /);
   $target=$att[1];
   $target=~s/ sequence //;
  }
  elsif($fields[2] eq 'exon'){
   print "$fields[0]\t$fields[1]\test2genome_match\t".join("\t",@fields[3..7])."\tID=exonerate_match_$count;Target=$target\n";
  }
 }
}
close IN;

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2015 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   parse a Exonerate GTF file producin output compatible with EvidenceModeller.

USAGE
    $0 --exonerate file.gtf

OPTIONS
    --Exonerate -e     Exonerate results file.  REQUIRED
    --help,     -h     This help.
    --license,  -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2015 Diego Mauricio Riaño Pachón
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

