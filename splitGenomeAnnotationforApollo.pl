#!/usr/bin/perl

use strict;
use warnings;
use Bio::SeqIO;
use Bio::DB::Fasta;
use Getopt::Long;

my $version="0.1";
my $fastafile='';
my $gfffile='';
my $idgroups='';
my $license='';
my $help='';
my $maxNumberSeqs=30000;

############################################
#Get input from user
############################################
GetOptions(
    'license|l'       => \$license,
    'help|h|?'        => \$help,
    'fastafile|f:s'   => \$fastafile,
    'groups|p:s'      => \$idgroups,
    'gfffile|g=s'     => \$gfffile,
);

############################################
#Check input from user
############################################
if($help){
 &usage;
 exit 1
}
if($license){
 &license;
 exit 1
}
if(!-s $fastafile && !-s $idgroups){
 print STDERR "\n\tFATAL:  You must provide either a multifasta genome file or an ID groups file.\n\n";
 &usage;
 exit 0;
}
if(!-s $gfffile){
 print STDERR "\n\tFATAL: You must provide a file with the list of sequence identifiers\n\n";
 &usage;
 exit 0;
}

########################################
## Process fasta file or groups file
########################################
my $mapGroupBySeqs='';
my $mapSeqByGroups='';
if(-s $fastafile){
 ($mapSeqByGroups,$mapGroupBySeqs)=splitFasta($fastafile);
}
elsif(-s $idgroups){
 ($mapSeqByGroups,$mapGroupBySeqs)=getGroups($idgroups);
}
else{
 print STDERR "You must provide either a multifasta genome file or an ID groups file.\n\n";
 &usage;
 exit 0;
}

########################################
## Process GFF file
########################################

#Creat output files

open GFF, $gfffile;
my %splitGFF;
while(<GFF>){
 next if /^#/;
 my @fields=split(/\t/);
 if($$mapGroupBySeqs{$fields[0]}){
#  print "$fields[0]\t$$mapGroupBySeqs{$fields[0]}\n";
  $splitGFF{$$mapGroupBySeqs{$fields[0]}}.=$_;
 }
}
close GFF;

foreach my $group( sort { $a <=> $b } keys %{$mapSeqByGroups}){
 print "$group\n";
 my $outGff=$gfffile.".subgroup".$group.".gff3";
 open GFFOUT, ">$outGff";
 print GFFOUT $splitGFF{$group}."\n";
 close GFFOUT;
}

########################################
## Subroutines
########################################

sub splitFasta{
 my $fasta=shift;
 my $db = Bio::DB::Fasta->new($fasta); 
 my @ids = sort { $a cmp $b } $db->get_all_primary_ids;
 my $groupCount=1;
 my $mapFile=$fasta.".groups.tbl";
 open MAP, ">$mapFile";
 print MAP "#SeqID\tGroup\n";
 for (my $i=0; $i<@ids;$i++){
  print MAP "$ids[$i]\t$groupCount\n";
  if($i != 0 && (($i)%$maxNumberSeqs) == 0 ){
   $groupCount++;
  }
 }
 close MAP;
 my ($mapSeqByGroups,$mapGroupBySeqs)=getGroups($mapFile,1);
 foreach my $group(sort {$a <=> $b} keys %$mapSeqByGroups){
  my $outSeqfile=$fasta.".subgroup".$group.".fasta";
  print "\t$outSeqfile\n";
  my $outSeqObj = Bio::SeqIO->new(-file => ">$outSeqfile", -format => 'FASTA');
  foreach my $id( sort { $a cmp $b } keys %{$$mapSeqByGroups{$group}}){
   my $seqObj = $db->get_Seq_by_id($id);
   $outSeqObj->write_seq($seqObj);
  }
 }
 return $mapSeqByGroups,$mapGroupBySeqs;
}

sub getGroups{
 my $list=shift;
 my $mode=shift;
 open MAP, $list;
 my %gSbG;
 my %gGbS;
 while(<MAP>){
  next if(/^#/);
  chomp;
  my ($id,$group)=split(/\t/);
  $gSbG{$group}{$id}=1;
  $gGbS{$id}=$group;
 }
 
 return \%gSbG, \%gGbS;
}

############################################
#Usage
############################################
sub usage{
    print STDERR "$0 version $version, Copyright (C) 2015 Diego Mauricio Riano Pachon\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   split a multifasta file and a GFF accordingly. Additionally produces a file with the sequence ids in each splitted file.

USAGE
    $0 --fastafile file.fasta --gfffile file.txt
     or
    $0 --groups group.file --gfffile file.txt

OPTIONS
    --fastafile -f    Multifasta genome file.
    --groups    -p    Group file (SeqID[TAB]Group.
                       One of fastafile or groups MUST be provided.
    --gfffile   -g    List of identifiers, and group one per line. REQUIRED
    --help      -h    This help.
    --license   -l    License.

EOF
}
############################################
#License
############################################
sub license{
    print STDERR <<EOF;

Copyright (C) 2015 Diego Mauricio Riaño Pach<C3>
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}


