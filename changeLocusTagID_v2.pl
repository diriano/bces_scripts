#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

#my $infile=$ARGV[0];
my $tbl='';
my $ntbl='';
my $locus_tag='';
my $subversion='';
my $help='';
my $license='';
my $version='0.1';

GetOptions ("tbl=s"            => \$tbl,
            "newtbl=s"         => \$ntbl,
            "locus_tag|lt=s"   => \$locus_tag,
            "sub_version=s"    => \$subversion,
            "help|h"           => \$help,
            "license|l"        => \$license
)            
or die("Error in command line arguments\n");

if($license) {
 license();
 exit(1);
}

if($help) {
 usage();
 exit(1);
}

if(!$tbl) {
 print "You must specify input file (.tbl for correction of LOCUSTAG)\n\n";
 usage();
 exit(1);
}

if(!$ntbl) {
 print "You must specify an output file (new .tbl)\n\n";
 usage();
 exit(1);
}

if(-e $ntbl) {
 print "Outfile already exists ($ntbl)\n\n";
 usage();
 exit(1);
}

if(!$locus_tag) {
 print "You must specify a locus tag (such as \"GLUCOINTAF2\")\n\n";
 usage();
 exit(1);
}

if(!$subversion) {
 print "You must specify a new version for the .tbl file (\"02\" if you are submitting the assembly's second version)";
 usage();
 exit(1);
}

open(INFILE,$tbl);

while(<INFILE>) {
 chomp;
 if($_ =~ /locus\_tag/ and not $_ =~ /$locus_tag/) {
  print "It seems that your locus tag ($locus_tag) specified in command-line is different from the one in old tbl.\nCheck this line:\n$_\n";
  exit(1);
 }
}

close(INFILE);

open(INFILE,$tbl);
open(OUTFILE,">>",$ntbl);

while(<INFILE>) {
 chomp;
 if($_ =~ /$locus_tag\_/ && $_ =~ /locus\_tag/) {
  $_ =~ s/$locus_tag\_(\d{5})/$locus_tag\_$subversion$1/g;
  print OUTFILE "$_\n";
 }
 elsif ($_ =~ /$locus_tag\_/ && $_ =~ /protein\_id/) {
  $_ =~ s/$locus_tag\_(\d{5})/$locus_tag\_$subversion$1/g;
  print OUTFILE "$_\n";
 } else {
  print OUTFILE "$_\n";
 }
}

close(INFILE);
close(OUTFILE);

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2015 Renato Augusto Corrêa dos Santos\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0 

USAGE
    $0 --tbl bacterium.tbl --newtbl bacterium.new.tbl --locus_tag GLUCOINTAF2 --sub_version 02

OPTIONS
    --tbl                Old .tbl file        REQUIRED
    --newtbl             New .tbl file        REQUIRED
    --locus_tag   --lt   Locus Tag            REQUIRED
    --sub_version        Submission version   REQUIRED
    --help,       -h     This help.
    --license,    -l     License.
EOF
}

sub license{

print STDERR <<EOF;

Copyright (C) 2015 Renato Augusto Corrêa dos Santos
http://bce.bioetanol.org.br/
e-mail: renato.santos\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}
