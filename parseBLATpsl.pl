#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;
use Getopt::Long;

#Set global vars
my $version = 0.1;
my $blat_file= '';
my $help='';
my $license='';
my $minCoverage=0.5;
#Set options
GetOptions
  (
   'blat|b=s'       => \$blat_file,
   'help|h|?'       => \$help,
   'license|l'      => \$license,
  )
  or die "failed to parse command line options\n";

#Check input from user
if($help){
 usage();
 exit 0;
}
if($license){
 license();
 exit 0;
}
if(!-s $blat_file){
 warn "FATAL: You must provide a file with the blat (psl format) results\n";
 usage();
 exit 1;
}

open BLAT, $blat_file;
my @blat=<BLAT>;
close BLAT;

if ($blat[0] ne "psLayout version 3\n"){
 die "This should be a BLAT file. It does not look that way, Check your input!\n"
}

my $count=1;
#psLayout version 3
#
#match	mis- 	rep. 	N's	Q gap	Q gap	T gap	T gap	strand	Q        	Q   	Q    	Q  	T        	T   	T    	T  	block	blockSizes 	qStarts	 tStarts
#     	match	match	   	count	bases	count	bases	      	name     	size	start	end	name     	size	start	end	count
#-------------------------------------------
$|=1;
for (my $i=0;$i<@blat;$i++){
 chomp $blat[$i];
 next if ($blat[$i]=~/^psLayout/);
 next if ($blat[$i]=~/^match/);
 next if ($blat[$i]=~/^\s+match/);
 next if ($blat[$i]=~/^-------/);
 next if ($blat[$i] eq '');
# print $blat[$i]."\n";
 my ($match,$mismatch,$rep,$n,$qgapcount,$qgapbases,$tgapcount,$tgapbases,$strand,$qname,$qsize,$qstart,$qend,$tname,$tsize,$tstart,$tend,$blockCount,$blockSizes,$qstarts,$tstarts)=split(/\s+/, $blat[$i]); 
 if($strand eq '++'){$strand='+'}
 elsif($strand eq '+-'){$strand='-'}
 my $qlengthAligned=$qend-$qstart+1;
 my $qlengthAlignedFraction=$qlengthAligned/$qsize;
 my $identity=$match/$qlengthAligned;
 if($qlengthAlignedFraction>=$minCoverage && $match>=$qlengthAligned/2){
#  print "SELECTED: ".$blat[$i]."\n";
  my @sizes=split(/,/,$blockSizes);
  my @tstarts=split(/,/,$tstarts);
  my $matchID=$tname."_match".$count;
  my $start=$tstart+1;
  print "$tname\tBLAT\tmatch\t$start\t$tend\t\.\t$strand\t\.\tID=$matchID;Query=$qname;QueryCoverage=".sprintf("%.2f",$qlengthAlignedFraction).";Identity=".sprintf("%.2f",$identity)."\n";
  for (my $i=0;$i<@tstarts;$i++){
   my $fragstart=$tstarts[$i]+1;
   my $fragend=$tstarts[$i]+$sizes[$i];
   if($strand eq '+'){
    print "$tname\tBLAT\tmatch_part\t".$fragstart."\t".$fragend."\t\.\t$strand\t\.\tParent=$matchID\n";
   }
   elsif($strand='-'){
    my $fixEnd  =$tsize-$tstarts[$i];
    my $fixStart=$fixEnd-$sizes[$i];
    print "$tname\tBLAT\tmatch_part\t".$fixStart."\t".$fixEnd."\t\.\t$strand\t\.\tParent=$matchID\n";
   }
   else{
    die "Do not recognize strand $strand\n";
   }
  }
  $count++;
 }
}

sub usage{
    print STDERR "$0 version $version, Copyright (C) 2015 Diego Mauricio Riaño Pachón\n";
    print STDERR "$0 comes with ABSOLUTELY NO WARRANTY; for details type `$0 -l'.\n";
    print STDERR "This is free software, and you are welcome to redistribute it under certain conditions;\n";
    print STDERR "type `$0 -l' for details.\n";
    print STDERR <<EOF;
NAME
    $0   parse a BLAT psl file.

USAGE
    $0 --blat file.psl

OPTIONS
    --blat      -b     Genome sequence.            REQUIRED
    --help,     -h     This help.
    --license,  -l     License.
EOF
}

sub license{
    print STDERR <<EOF;

Copyright (C) 2015 Diego Mauricio Riaño Pachón
http://bce.bioetanol.org.br/
e-mail: diego.riano\@bioetanol.org.br

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
EOF
exit;
}

